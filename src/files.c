/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#include "figura.h"
#include "files.h"
#include "comandos.h"
#include "screen.h"
#include "snap.h" 
#include <gnome.h> 
#include <math.h> 
#include <dlfcn.h>
#include <string.h>
#include <SDL.h>

/***********************************************/
// Nombre: lcad_load_file          
//															  
// Comentarios: carga un archivo de datos   
// 
// Salida: bloque con figuras 
/**********************************************/

bloque *lcad_load_file(char *name,GList *lista)
{
	FILE *load;
	figura *fig;
	int i;
	bloque *bloque;
	GList *listaf;
	void *handle;

	
	bloque=jcc_bloque_new();
	fig=malloc(sizeof(figura));
	
	load=fopen(name,"r");
	
	if(load==NULL)	
	{
		return(0);
	}	


	while(!feof(load))
	{
	fig=crea_figura();
	
	//is_spline
	fread(&fig->is_spline,sizeof(int),1,load);
	//nodos
	fread(&fig->n_nodo,sizeof(int),1,load);
	fread(&fig->s_nodo,sizeof(int),1,load);
	fread(&fig->e_nodo,sizeof(int),1,load);
	//suavizada
	fread(&fig->correc,sizeof(int),1,load);
	//patron
	fread(&fig->patron,sizeof(int),1,load);
	//longitud
	fread(&fig->longitud,sizeof(double),1,load);
	//color
	fread(fig->color,sizeof(float),3,load);
	//orden
	fread(&fig->orden,sizeof(int),1,load);
	//grosor
	fread(&fig->grosor,sizeof(float),1,load);
	//centro
	fread(&fig->centro,sizeof(punto),1,load);
	//matrices de transformacion
	fread(&fig->matriz_rotacion,sizeof(double),16,load);
	fread(&fig->matriz_translacion,sizeof(double),16,load);
	//bases
	fread(&fig->base_fx,sizeof(base),1,load);
	fread(&fig->base_fy,sizeof(base),1,load);
	fread(&fig->base_fz,sizeof(base),1,load);
	//parametros
	fread(&fig->num_parametros_generadores,sizeof(int),1,load);
	fig->parametros_generadores=malloc(sizeof(punto)*(fig->num_parametros_generadores+1));
	fread(fig->parametros_generadores,sizeof(punto)*(fig->num_parametros_generadores+1),1,load);
	//funcion
	fread(&fig->TIPO,sizeof(int),1,load);
	fread(fig->func_name,sizeof(char),20,load);

	handle=dlopen("metodos.o",RTLD_LAZY);
	fig->func=dlsym(handle,fig->func_name);
	if(fig->is_spline==1)
		{
		//g_print("\nspline");
		fig->longitud=fig->n_nodo;
		i=1+fig->n_nodo;
		fig->base_fx.cubic=malloc(sizeof(cubic_spl)*i);
		fig->base_fy.cubic=malloc(sizeof(cubic_spl)*i);
		fig->base_fz.cubic=malloc(sizeof(cubic_spl)*i);

		fread(fig->base_fx.cubic,sizeof(cubic_spl)*i,1,load);
		fread(fig->base_fy.cubic,sizeof(cubic_spl)*i,1,load);
	  	fread(fig->base_fz.cubic,sizeof(cubic_spl)*i,1,load);
		
 //g_print("\nleido %f\n",fig->base_fx.cubic[1].b_monomios[2]);
		}
	
	if(fig->n_nodo<=1000&&fig->n_nodo!=0)
		{
		listaf=g_list_first(lista);
		listaf=g_list_append(listaf,fig);
		calcula_puntos_precalculados(fig,0,fig->longitud);
		jcc_bloque_addFigure(bloque,fig);	
		if(lista!=lista_auxiliar)
			add_snaps(fig);
		//g_print("\nfig cargada->%d",fig->ID);
		}
	}
	fclose(load);
	return bloque;
}








void lcad_save_file(char *name,GList *lista)
{
FILE *save;
char hola[20];
figura *fig;
int i;
//g_print("archivo %s",name);
save=fopen(name,"w+");
GList *listaf;

listaf=g_list_next(lista);
//ng_print("listad-%d\n",listaf);

while (listaf!=NULL)
	{
	//g_print("OK");

	fig=listaf->data;
	//guardamos los datos de la figura
	
	//is_spline
	fwrite(&fig->is_spline,sizeof(int),1,save);
	//nodos
	fwrite(&fig->n_nodo,sizeof(int),1,save);
	fwrite(&fig->s_nodo,sizeof(int),1,save);
	fwrite(&fig->e_nodo,sizeof(int),1,save);
	//suavizada
	fwrite(&fig->correc,sizeof(int),1,save);
	//patron
	fwrite(&fig->patron,sizeof(int),1,save);
	//longitud
	fwrite(&fig->longitud,sizeof(double),1,save);
	//color
	fwrite(fig->color,sizeof(float),3,save);
	//orden
	fwrite(&fig->orden,sizeof(int),1,save);
	//grosor
	fwrite(&fig->grosor,sizeof(float),1,save);
	//centro
	fwrite(&fig->centro,sizeof(punto),1,save);
	//matrices de transformacion
	fwrite(&fig->matriz_rotacion,sizeof(double),16,save);
	fwrite(&fig->matriz_translacion,sizeof(double),16,save);
	//bases
	fwrite(&fig->base_fx,sizeof(base),1,save);
	fwrite(&fig->base_fy,sizeof(base),1,save);
	fwrite(&fig->base_fz,sizeof(base),1,save);
	//parametros_generadores
	fwrite(&fig->num_parametros_generadores,sizeof(int),1,save);
	fwrite(fig->parametros_generadores,sizeof(punto)*(fig->num_parametros_generadores+1),1,save);
	//funcion
	fwrite(&fig->TIPO,sizeof(int),1,save);
	//fwrite(fig->func_name,strlen(fig->func_name),1,save);
	fwrite(fig->func_name,sizeof(char),20,save);

//	g_print("func name=%s",fig->func_name);	

	//y el contenido de los punteros a la base de splines
	if(fig->is_spline==1)
		{
		i=1+fig->n_nodo;
		fwrite(fig->base_fx.cubic,sizeof(cubic_spl)*i,1,save);
	   fwrite(fig->base_fy.cubic,sizeof(cubic_spl)*i,1,save);
	   fwrite(fig->base_fz.cubic,sizeof(cubic_spl)*i,1,save);
		//g_print("base_spline guardada=%f",fig->base_fx.cubic[0].b_monomios[0]);
		}
	
	listaf=g_slist_next(listaf);
	
	}
	
	fclose(save);
}
