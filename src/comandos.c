/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
 
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <unistd.h>
#include "comandos.h"
#include <gnome.h> 
#include "figura.h"
#include "listas.h"
#include <dlfcn.h> 

#define SHM_SIZE    10000
#define SHM_MODE    (SHM_R | SHM_W)     /* read/write */


void *handle;

/***********************************************
	Function: command_not_found                     
**********************************************/

void command_not_found(void)
{
	texto_a_GUI("Comand not found",NULL);
}

/***********************************************
	Function: jcc_comandos_exec                     
**********************************************/

void jcc_comandos_exec(char *name)
{
	shmptr->comando=1;
	sprintf(shmptr->c_name,"new_%s",name);
}


/***********************************************
	Function: jcc_comandos_new                     
**********************************************/

int jcc_comandos_new(void)
{
	shmptr=malloc(sizeof(struct command));
	memset(shmptr,0,sizeof(struct command));
	shmptr->mouse_enabled=0;
	shmptr->comando=0;
	handle=dlopen((const char*)"",RTLD_LAZY);

	return 1;
}

/***********************************************
	Function: jcc_comandos_get                     
**********************************************/

int jcc_comandos_get(void)
{
	figura *(*func)();	
	const char pg_name[100];
	char gen_name[100];
	//Comprobando comandos enviados a shmptr->comando desde el GUI

	switch(shmptr->comando)
	{
		
		case 1:
//		sprintf((char*)pg_name,"/home/deckard/%s.so",shmptr->c_name);
//		handle=dlopen((const char*)pg_name,RTLD_LAZY);
		handle=dlopen((const char*)"",RTLD_LAZY);
		func=dlsym(handle,shmptr->c_name);
	   if(dlerror()!=NULL)
			{
				command_not_found();
				return 1;
			}
		func();
		proyecta_snap_points();
		dlclose(handle);
		break;		

		case savef:
		lcad_save_file(shmptr->ret_msg,lista_figuras);	
		proyecta_snap_points();
		break;
		
		case loadf:
		lcad_load_file(shmptr->ret_msg,lista_figuras);	
		proyecta_snap_points();
		break;
	
		case loaddxf:
		dxf_load_file(shmptr->ret_msg);	
		proyecta_snap_points();
		break;
	}	

	texto_a_GUI("Input new command",NULL);
	shmptr->comando=0;
	shmptr->n_param_to_read=0;
	shmptr->end_figure=0;
	return 0;
}
