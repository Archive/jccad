/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
 
#include "screen.h"
#include "figura.h"
#include <GL/gl.h>
#include <gnome.h>

/***********************************************
	Function: jcc_draw 
**********************************************/

void jcc_draw(GList *lista)
{
	//declaracion de variables
	GList *lista_act;
	double t;//parametro 
	punto pto,pto_pantalla;
	int i,j;
	int CPZ;//correccion por zoom
	double interv;
   figura *fig,*fig2;//figura auxiliar
	
	if(screen_info.drawing==1)
		return;

	screen_info.drawing=1;
	//limpiamos el visor si se debe
//	glDrawBuffer(GL_BACK);
	glClear(GL_COLOR_BUFFER_BIT);
	// selecionamos la matriz de proyeccion
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//esto crea una proyeccion ortonormal

	if(screen_info.select_state<2)	
		{
		glOrtho
			(-(1)*screen_info.zoom, 
			(1)*screen_info.zoom, 
			(-1)*screen_info.zoom, 
			(1)*screen_info.zoom, -40000, 40000);
		if(screen_info.select_state==1)
			dibuja_seleccion();
		}	
	else
		{
		glOrtho((screen_info.select_point_begin.x), 
			(screen_info.select_point_end.x), 
			(screen_info.select_point_begin.y), 
			(screen_info.select_point_end.y), -40000, 40000);
		
		}

	
	glTranslatef(-screen_info.scroll_x,-screen_info.scroll_y,0);

	//giro de camara
	glRotatef(screen_info.angle[0],1,0,0);
	glRotatef(screen_info.angle[1],0,1,0);
	glRotatef(screen_info.angle[2],0,0,1);

	glGetFloatv(GL_PROJECTION_MATRIX,screen_info.proyeccion);
	
	if(screen_info.select_state<2)	
	{
	//dibuja figuras auxiliares (cursor,puntos...)	
	jcc_aux_draw();
	}

	//seleccionamos el primer elemento de la lista de figuras
	lista_act=g_list_next(lista);
	glMatrixMode(GL_MODELVIEW);
	glInitNames();	
	//por cada elemento de la lista...	
   
	while (lista_act !=NULL)
		{
		fig=lista_act->data;
		

		glLoadIdentity();
		
		glLineWidth(fig->grosor);
		
		glLoadName(fig);	
		glPushName(fig);
		
		if(fig->patron!=NULL)
			{
			glLineStipple (1,fig->patron);
			}
		else glLineStipple (1,0xFFFF);

		
		if(fig->selected==0)	
			glColor4f(fig->color[0], fig->color[1],fig->color[2],fig->color[3]);
		else	
			{
			glColor4f(0,0,1,1);
			glLineStipple (1,0x0AFF);
			}
			
		glBegin(GL_LINE_STRIP);
			
			for (i = 0; i < fig->num_puntos_precalculados; i++)
			{

				glVertex3f(fig->puntos_precalculados[i].x,
					fig->puntos_precalculados[i].y,
					fig->puntos_precalculados[i].z);
			}
	
		glEnd();
		
		lista_act=g_slist_next(lista_act);
		
		}		

	glPopAttrib ();
	SDL_GL_SwapBuffers();
	screen_info.drawing=0;
}

