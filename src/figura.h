/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#ifndef _FIGURA_H
#define _FIGURA_H

#include "punto.h"
#include "layer.h"
#include "listas.h"
#include <gnome.h>

//BASE DE SPLINES CUBICOS

struct cubic_spl
{
double P1;
double P2;
double T1;
double T2;
double b_monomios[4];
};

typedef struct cubic_spl cubic_spl;



//Estructura de base debil de la funcion, declaramos arrais, en donde el indice
// de este indica el grado a la que esta elevado el elemento 
// de la base.cada valor introducido sera el coeficiente por
// el que se multiplica el elemento de la base. 
//La funcion resultante es la combinacion lineal de todos
//los elementos de la base. 

struct base
{
double b_monomio[5];//base de monomios hasta grado 20
double seno[5];//base de senos hasta grado 20
double coseno[5];//base de cosenos hasta grado 20
cubic_spl *cubic;
};

typedef struct base base;

//declaracion de figura


typedef struct figuras figura;

struct figuras{
	int ID;
	int TIPO;
	int is_spline; //indica si se va a usar la base de cubicas
	int capa;//capa donde se encuentra
//////////
int n_nodo;//numero de nodos de control en la figura
int s_nodo;//nodo inicial
int e_nodo;//nodo final
//////////
	int correc;//suavizada
	int patron;//tipo de linea (continua,punteada...)
	int can_be_selected;
	int selected;
	double t_inicial;
	double longitud; //parametro inicial-parametro final
	float color[3];//color de la figura
	int orden;//orden maximo de las ecuaciones parametricas
	float grosor;//grosor de linea
	punto centro; //centro de la figura
	double x_max,y_max,z_max; 
	double x_min,y_min,z_min; //delimita la figura 
	double matriz_translacion[16]; //Matriz de transformacion de la figura
	double matriz_rotacion[16];
	double matriz_escala[16];
	double matriz_translacion_i[16]; //Matriz de transformacion de la figura
	double matriz_rotacion_i[16];
	double matriz_escala_i[16];
	base   base_fx; //bases para las ecuaciones parametricas de la figura
	base   base_fy;
	base   base_fz;
	punto *puntos_precalculados;//puntos precalculados para acelerar representacion
	punto *parametros_generadores;//parametros para generar la figura
	struct bloque *aux_fig;
	int num_parametros_generadores;//numero de parametros
	int opciones_generadoras[4];//parametros para generar la figura
	int num_opciones_generadoras;//numero de parametros
	int num_puntos_precalculados;//numero de puntos precalculados
	int metodo_generador;//metodo de creacion de la figura
	GList *bloques;//bloques en donde esta agrupada la figura
	layer *layer_fig;
	figura (*func)();
	char func_name[20];
};

//metodos
figura *crea_figura(void);
void destruye_figura(figura *fig);
void calcula_puntos_precalculados(figura *fig,double t_i,double t_f);
int calcular_interseccion(figura *fig1,figura *fig2);
punto calcula_punto(double t,figura *fig);
int comprueba_figura(figura *figi,GList *lista);
figura *copia_figura(figura *fig,GList *lista);
void translada_figura(figura *fig,punto base,punto destino);
void jcc_entity_rotate(figura *fig,punto base,double angle);
#endif
