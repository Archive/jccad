/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
 
#ifndef _LINE_H
#define _LINE_H

#include <glib.h>
#include "metodos.h"
#include "math.h"
#include "SDL.h"
#include <SDL_thread.h>


//la siguiente funcion asocia una linea a la figura
figura eqlinea(figura fig,punto *ptos,int orden)
{
fig.base_fx.b_monomio[0]=ptos[0].x;
fig.base_fy.b_monomio[0]=ptos[0].y;
fig.base_fz.b_monomio[0]=ptos[0].z;

//if(orden>0)
//	{
	fig.base_fx.b_monomio[1]=(ptos[1].x-ptos[0].x)/fig.longitud;
	fig.base_fy.b_monomio[1]=(ptos[1].y-ptos[0].y)/fig.longitud;
	fig.base_fz.b_monomio[1]=(ptos[1].z-ptos[0].z)/fig.longitud;
//	}
fig.orden=1;

fig.correc=0;
fig.n_nodo=1;
fig.s_nodo=0;
fig.e_nodo=1;

return fig;
}



void new_line(void)
{
	int com;
	figura *fig;
	
	fig=crea_figura();
	lista_figuras=g_list_append(lista_figuras,fig);
	
	fig->color[0]=shmptr->color_r;
	fig->color[1]=shmptr->color_g;
	fig->color[2]=shmptr->color_b;
	
	fig->parametros_generadores=malloc(sizeof(punto)*3);
	fig->func=eqlinea;
	
//	fig->TIPO=linea;
	sprintf(fig->func_name,"eqlinea");	

	do{
//comprueba el numero de parametros recogidos en fig
	com=fig->num_parametros_generadores;
	texto_a_GUI("Introduzca punto %d de la linea",com+1);
	shmptr->is_msg=1;
//cogemos los parametros introducidos desde la linea de comandos
	get_parameters_from_input(fig->func,com,fig);
//y tambien desde el raton
	get_parameters_from_mouse(fig->func,fig,com);
//2 puntos
	}while(com<=1);

//	fig->num_parametros_generadores--;
	
	shmptr->n_param_to_read=0;

	add_snaps(fig);

	return fig;
}
#endif
