/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
 	
#ifndef _COMANDOS_H
#define _COMANDOS_H

#include "punto.h"
#include "layer.h"

//declaracion de comandos
#define savef 2001
#define loadf 2002
#define loaddxf 2003

char *parametros_texto[2];
//estructura en espacio de memoria compartida
//sirve de comunicacion con el interfaz 
struct command 
{
int comando;
char c_name[20];
punto parametros[200];//pila de parametros para generar la figura
punto ultimo_parametro;
layer_state layer_s;
int options[5];//opciones para generar la figura
int n_param_to_read;//numero de parametros
int n_opts_to_read;//numero de opciones
int linetype;
int lwidth;
double color_r;
double color_g;
double color_b;
int snap_intersection;
int snap_orto;
int snap_end_point;
int snap_mid_point;
int snap_center_point;
int snap_s_point;
char comando_in[100];
char ret_msg[100];//mensaje a mostrar por la ventana de comandos
int is_msg;
int mouse_enabled;
int end_figure;
int quit;
punto cursor;
};

struct command *shmid, *shmptr; //para la memoria compartida

//metodos
int jcc_comandos_new(void);
void jcc_comandos_exec(char *name);
int jcc_comandos_get(void);
#endif
