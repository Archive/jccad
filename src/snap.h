/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#ifndef _snap_H
#define _snap_H

#include "figura.h"
#include "punto.h"
#include "bloque.h"


struct snap
{
punto punto;
punto p_proyected;
int ID_FIGURA[2];
double PRM_FIGURA[2];
double snap_dist;
bloque *auxiliar;
};

typedef struct snap snap;

struct snap_points  
{
snap *grid;
int n_grid;
GList *intersection;
GList *end_points;
GList *mid_points;
GList *control_points;
GList *center_points;
GList *s_points;
snap grid_points[200000];
int n_grid_points;
}snap_points;


//metodos
void add_snaps(figura *fig);
void add_snap_intersection(figura *fig,double t1,figura *fig2,double t2,punto i_point);
void add_snap_end_point(figura *fig);
void add_snap_center_point(figura *fig);
void add_snap_control_point(figura *fig,punto control_point,int n_punto);
void add_snap_grid_point(int size);
void snap_orto(punto last_point,punto *mouse_pos);
void snap_intersec(punto *mouse_pos);
void snap_end_point(punto *mouse_pos);
void snap_mid_point(punto *mouse_pos);
void get_snap_points(punto *mouse_pos);
void situa_snap_control_point(figura *fig,punto control_point,int n_punto);
snap snap_control_point(punto *mouse_pos);
void remove_s_snap_points(figura *fig);
void destroy_snap_points(GList *lista,figura *fig);
#endif
