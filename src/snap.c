/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#include "SDL.h"
#include <gnome.h>
#include "auxiliares.h"
#include "snap.h"
#include "matriz_op.h"
#include "comandos.h"
#include "screen.h"
#include "listas.h"
#include "math.h"
#include "bloque.h"
#include "files.h"



void proyecta_snap_points(void)
{
	GList *lista;
	snap *snap;
	punto *pto;
	
	double *l;
	punto pp,pp2;

	
	lista=snap_points.intersection;
	lista=g_list_next(lista);

	while(lista !=NULL)
		{
			snap=lista->data;
			snap->p_proyected=calcula_ppoint(snap->punto);
			lista=g_list_next(lista);
		}

	lista=snap_points.end_points;
	lista=g_list_next(lista);

	while(lista !=NULL)
		{
			snap=lista->data;
			snap->p_proyected=calcula_ppoint(snap->punto);
			lista=g_list_next(lista);
		}

	lista=snap_points.mid_points;
	lista=g_list_next(lista);

	while(lista !=NULL)
		{
			snap=lista->data;
			snap->p_proyected=calcula_ppoint(snap->punto);
			lista=g_list_next(lista);
		}

	lista=snap_points.center_points;
	lista=g_list_next(lista);

	while(lista !=NULL)
		{
			snap=lista->data;
			snap->p_proyected=calcula_ppoint(snap->punto);
			lista=g_list_next(lista);
		}

	lista=snap_points.s_points;
	lista=g_list_next(lista);

	while(lista !=NULL)
		{
			snap=lista->data;
			snap->p_proyected=calcula_ppoint(snap->punto);
			lista=g_list_next(lista);
		}
}


void proyecta_snap_control_points(void)
{
	GList *lista;
	snap *snap;
	lista=g_list_first(snap_points.control_points);
	lista=g_list_next(lista);
	
	
	while(lista !=NULL)
	{
		snap=lista->data;
		snap->p_proyected=calcula_ppoint(snap->punto);
		lista=g_list_next(lista);
	}
}

snap *get_min_distance_alt(punto mouse_pos,GList *lista)
{
	snap *ret,ret_t;
	snap *snaps;
	snap ret_m; 
	GList *lista_act;
	double min_distance,distance;
	
	lista_act=g_list_first(lista);
	lista_act=g_list_next(lista_act);
  	
	
	ret_m.punto=mouse_pos;
	ret_m.ID_FIGURA[0]=0;
	ret_m.ID_FIGURA[1]=0;
	
	ret=&ret_m;
	
	min_distance=screen_info.zoom;
//	g_print("\nmouse_pos %f,%f,%f",mouse_pos.x,mouse_pos.y,mouse_pos.z);
	
	while(lista_act !=NULL)
		{
			snaps=lista_act->data;

			distance=sqrt((pow((snaps->p_proyected.x-ret_m.punto.x),2)+pow((snaps->p_proyected.y-ret_m.punto.y),2)));
//			distance=abs(snaps->p_proyected.x-ret_m.punto.x)+abs(snaps->p_proyected.y-ret_m.punto.y);
			
			
			if(distance<min_distance&&distance<snaps->snap_dist*0.5*screen_info.zoom)
			{
				min_distance=distance;
				jcc_aux_move(snap_aux,snaps->punto);
				if(distance<snaps->snap_dist*0.05*screen_info.zoom)
				{
					jcc_aux_move(cursor,snaps->punto);			   
					return snaps;	
				}
			}
			
			lista_act=g_list_next(lista_act);
		}
		
		if(min_distance<10000)
		{
			ret_t=*ret;
			return &ret_t;
		}
		else return &ret_m;
}

//CALCULA LA MENOR DISTANCIA EUCLIDEA ENTRE UN CONJUNTO DE PUNTOS
snap get_min_distance(punto mouse_pos,snap *snaps,int n_puntos)
{
/*	snap ret;
	double min_distance=100000,distance;
	int i=0,snap=NULL;
	
	do
	{
		//eliminando raiz cuadrada para acelerar calculos
		distance=pow((snaps[i].punto.x-mouse_pos.x),2)+pow((snaps[i].punto.y-mouse_pos.y),2);
		if(distance<min_distance)
		{
			min_distance=distance;
			snap=i;
		}
		i++;
	}while(i<=n_puntos);
*/	
		/*situa_snap_auxiliar(snaps[snap].punto,2);*/
/*	
	if(min_distance<(snaps[snap].snap_dist)*screen_info.zoom) 
		{
		return snaps[snap];
		}else 
			{
			ret.punto=mouse_pos;
			ret.ID_FIGURA[0]=0;
			return ret;
 			}*/
}

//A�ADE SNAP POINTS
void add_snap_intersection(figura *fig1,double t1,figura *fig2,double t2,punto i_point)
{
	GList *lista;
	snap *intersection;
	
	lista=snap_points.intersection;

	intersection=malloc(sizeof(snap));
//	i_point.z-=i_point.y+i_point.z;
	intersection->punto=i_point;
	intersection->ID_FIGURA[0]=fig1->ID;
	intersection->ID_FIGURA[1]=fig2->ID;
	intersection->PRM_FIGURA[0]=t1;
	intersection->PRM_FIGURA[1]=t2;
	intersection->snap_dist=1;
	lista=g_list_append(lista,intersection);
}

void add_snap_end_point(figura *fig)
{
	GList *lista;
	snap *begin_points;
	snap *end_points;
	
	lista=snap_points.end_points;

	begin_points=malloc(sizeof(snap));
	end_points=malloc(sizeof(snap));
	
	end_points->punto=fig->puntos_precalculados[0];
	end_points->ID_FIGURA[0]=fig->ID;
	end_points->snap_dist=1;
	begin_points->punto=fig->puntos_precalculados[fig->num_puntos_precalculados-1];
	//g_print("\n alt:%f",begin_points->punto.z);
	begin_points->ID_FIGURA[0]=fig->ID;
	begin_points->snap_dist=1;
	lista=g_list_append(lista,begin_points);
	lista=g_list_append(lista,end_points);
}

void add_snap_mid_point(figura *fig)
{
	GList *lista;
	snap *mid_points;
	
	lista=snap_points.mid_points;
	mid_points=malloc(sizeof(snap));
	mid_points->punto=calcula_punto(fig->longitud/2,fig);
	mid_points->ID_FIGURA[0]=fig->ID;
	mid_points->snap_dist=1;
	lista=g_list_append(lista,mid_points);
}

void add_snap_center_point(figura *fig)
{
	GList *lista;
	snap *center_points;

	lista=snap_points.center_points;
	center_points=malloc(sizeof(snap));
	center_points->punto=fig->centro;
	center_points->ID_FIGURA[0]=fig->ID;
	center_points->snap_dist=1;
	lista=g_list_append(lista,center_points);
}

void add_snap_s_point(figura *fig,punto s_point)
{
	GList *lista;
	snap *s_points;
	
	lista=snap_points.s_points;
	s_points=malloc(sizeof(snap));
	s_points->punto=fig->puntos_precalculados[0];
	s_points->ID_FIGURA[0]=fig->ID;
	s_points->snap_dist=1;
	lista=g_list_append(lista,s_points);
}


void add_snap_control_point(figura *fig,punto control_point,int n_punto)
{
	GList *lista;
	snap *control;
	bloque *aux;

	lista=g_list_first(snap_points.control_points);

	control= malloc(sizeof(snap));
	control->punto=control_point;
	control->ID_FIGURA[0]=fig->ID;
	control->ID_FIGURA[1]=n_punto;
	control->snap_dist=1;

 	control->auxiliar=jcc_bloque_copy(control_aux,lista_auxiliar);
	jcc_aux_move(control->auxiliar,control->punto);

	lista=g_list_append(lista,control);
}



void add_snap_grid_point(int size)
{
	int i,j;

	for(i=0;i<=100;i++)
	{
		for(j=0;j<=100;j++)
		{
			snap_points.n_grid_points++;
			snap_points.grid_points[snap_points.n_grid_points].punto.x=i*5;
			snap_points.grid_points[snap_points.n_grid_points].punto.y=j*5;
			snap_points.grid_points[snap_points.n_grid_points].snap_dist=10;
		}
	}	
}


//SITUAR MOUSE EN SNAPS POINTS
void snap_intersec(punto *mouse_pos)
{
	snap *snap_pto;
	
	snap_pto=get_min_distance_alt(*mouse_pos,snap_points.intersection);	
	*mouse_pos=snap_pto->punto;
}

void snap_end_point(punto *mouse_pos)
{
	snap *snap_pto;
	//g_print("\nmouse_pos %f,%f,%f",mouse_pos->x,mouse_pos->y,mouse_pos->z);
	snap_pto=get_min_distance_alt(*mouse_pos,snap_points.end_points);	
	*mouse_pos=snap_pto->punto;
}

void snap_mid_point(punto *mouse_pos)
{
	snap *snap_pto;
	snap_pto=get_min_distance_alt(*mouse_pos,snap_points.mid_points);	
	*mouse_pos=snap_pto->punto;
}

void snap_center_point(punto *mouse_pos)
{
	snap *snap_pto;
	snap_pto=get_min_distance_alt(*mouse_pos,snap_points.center_points);	
	*mouse_pos=snap_pto->punto;
}

void snap_grid_point(punto *mouse_pos)
{
	snap snap_pto;
//	snap_pto=get_min_distance(*mouse_pos,snap_points.grid_points,snap_points.n_grid_points);	
	*mouse_pos=snap_pto.punto;
}

void snap_s_point(punto *mouse_pos)
{
	snap *snap_pto;
	snap_pto=get_min_distance_alt(*mouse_pos,snap_points.s_points);	
	*mouse_pos=snap_pto->punto;
}

snap snap_control_point(punto *mouse_pos)
{
	snap *snap_pto;
	snap copy;

	snap_pto=get_min_distance_alt(*mouse_pos,snap_points.control_points);	
	copy=*snap_pto;
	*mouse_pos=snap_pto->punto;
	return copy;
}

//GET SNAP POINTS
void get_snap_points(punto *mouse_pos)
{
	punto mouse;

	mouse=*mouse_pos;
	
	if(shmptr->snap_intersection==1)
		snap_intersec(&mouse);
	
	if(shmptr->snap_end_point==1)
		snap_end_point(&mouse);

	if(shmptr->snap_mid_point==1)
		snap_mid_point(&mouse);
	
	if(shmptr->snap_center_point==1)
		snap_center_point(&mouse);

	if(shmptr->snap_s_point==1)
		snap_s_point(&mouse);

	*mouse_pos=mouse;
	shmptr->cursor=mouse;
}


//ORTO SNAP
void snap_orto(punto last_point,punto *mouse_pos)
{
	double x,y,tg;

	if(shmptr->snap_orto==1)
	{
		x=fabs(last_point.x-mouse_pos->x);
		y=fabs(last_point.y-mouse_pos->y);
		
		tg=x/y;
		
		if(fabs(tg)>=10||y==0)
		{
			mouse_pos->y=last_point.y;
		}
		else if(fabs(tg)<=0.1)
		{
			mouse_pos->x=last_point.x;
		}
		jcc_aux_move(cursor,*mouse_pos);
		shmptr->cursor=*mouse_pos;
	}
}


void remove_control_snap_points(figura *fig)
{

	destroy_snap_points(snap_points.control_points,fig);
}

void remove_s_snap_points(figura *fig)
{
	destroy_snap_points(snap_points.s_points,fig);
}

void remove_all_snap_points(figura *fig)
{
	destroy_snap_points(snap_points.intersection,fig);
	destroy_snap_points(snap_points.end_points,fig);
	destroy_snap_points(snap_points.mid_points,fig);
}

void destroy_snap_points(GList *lista,figura *fig)
{
	snap *snap;
	GList *lista_act;
	
	lista_act=g_list_first(lista);
	lista_act=g_list_next(lista_act);
  	
	while(lista_act!=NULL)
	{
  		snap=lista_act->data;
		if(snap->ID_FIGURA[0]==fig->ID||snap->ID_FIGURA[1]==fig->ID)
		{
			lista_act=g_list_remove(lista_act,snap);
		 	if(lista==snap_points.control_points)
			{	
				elimina_auxiliar(snap->auxiliar);
			}
			free(snap);
		}
		else{lista_act=g_list_next(lista_act);}
	}
}

void add_snaps(figura *fig)
{
	int i;
	GList *lista;
	lista=g_slist_next(lista_figuras);

	add_snap_end_point(fig);
	add_snap_mid_point(fig);

	//delimita la figura
	fig->x_max=fig->puntos_precalculados[0].x;
	fig->x_min=fig->puntos_precalculados[0].x;
	fig->y_max=fig->puntos_precalculados[0].y;
	fig->y_min=fig->puntos_precalculados[0].y;
	fig->z_max=fig->puntos_precalculados[0].z;
	fig->z_min=fig->puntos_precalculados[0].z;

	for(i=1;i<fig->num_puntos_precalculados;i++)
		{
			if(fig->puntos_precalculados[i].x>fig->x_max)
				fig->x_max=fig->puntos_precalculados[i].x;
			
			if(fig->puntos_precalculados[i].x<fig->x_min)
				fig->x_min=fig->puntos_precalculados[i].x;
			
			if(fig->puntos_precalculados[i].y>fig->y_max)
				fig->y_max=fig->puntos_precalculados[i].y;
			
			if(fig->puntos_precalculados[i].y<fig->y_min)
				fig->y_min=fig->puntos_precalculados[i].y;
			
			if(fig->puntos_precalculados[i].z>fig->z_max)
				fig->z_max=fig->puntos_precalculados[i].z;
			
			if(fig->puntos_precalculados[i].z<fig->z_min)
				fig->z_min=fig->puntos_precalculados[i].z;
			
		}
//g_print("xmax=%f xmin=%f	ymax=%f ymin=%f	zmax=%f zmin=%f",fig->x_max,fig->x_min,fig->y_max,fig->y_min,fig->z_max,fig->z_min);
	
	while (lista!=NULL)
		{
			if(lista->data!=fig)
				{
					calcular_interseccion(fig,lista->data);
				}
			lista=g_list_next(lista);
		}
	//proyecta_snap_points();
	//proyecta_snap_control_points();
}


void situa_snap_control_point(figura *fig,punto control_point,int n_punto)
{
	GList *lista;
	snap *snap;

	
	lista=snap_points.control_points;
	lista=g_list_next(lista);
	while(lista!=NULL)
	{
   	snap=lista->data;
		if(snap->ID_FIGURA[0]==fig->ID&&snap->ID_FIGURA[1]==n_punto)
		{
			snap->punto=control_point;
			jcc_aux_move(snap->auxiliar,snap->punto);
		}
		
		lista=g_list_next(lista);
	}
	
}
