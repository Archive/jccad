/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#ifndef _SCREEN_H
#define _SCREEN_H
#include "punto.h"
#include "SDL.h"

//puntero a pantalla
SDL_Surface *screen;

//declaracion de screen_info
struct screen_info
{
int drawing;
int RES_X,RES_Y;
double scroll_x;
double scroll_y;
double zoom;
double angle[3];
int select_state;
float proyeccion[16];
/*
double URM_tr[16];
int URM_rt[16];
int Matrizp_giro[16];
punto normal_pp;*/
punto select_point_begin; 
punto select_point_end;
}screen_info;


#endif
