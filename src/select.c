/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#include "select.h"
#include "listas.h"
#include <GL/gl.h>
#include <SDL.h>
#include "screen.h"
#include "eventos.h"
#include "auxiliares.h"
#include "snap.h"
#include "matriz_op.h"

//funciones privadas
void indica_figura_seleccionada(figura *fig);


/***********************************************/
// Nombre: select_fig                    
//															  
// Comentarios: a�ade las figuras seleccionadas 
//		a la lista de seleccion
// 
// Salida: ninguna 
/**********************************************/

void select_fig(GList *lista)
{
	figura *fig1;
	GList *lista_act;
	int i,n;
   int fig_sel[200000];
	int viewport[4];
	int hits;
	
	//buffer de seleccion
	glSelectBuffer (200000, fig_sel); 
	glMatrixMode(GL_PROJECTION);
	glGetIntegerv (GL_VIEWPORT,viewport);
	gluPickMatrix(0,0,1,1,viewport);
	
	glRenderMode (GL_SELECT); 	

	jcc_draw(lista_figuras);
	
	hits=glRenderMode(GL_RENDER);
	
	if(screen_info.select_state==2)
		deselect_fig();
	
	lista_act=g_list_first(lista_figuras);
	lista_act=g_list_next(lista_act);
	
	n=0;
	for(i=1;i<=hits;i++)
		{
			n+=fig_sel[n]+2;
			
			indica_figura_seleccionada(fig_sel[n]);
			lista_seleccion=g_list_append(lista_seleccion,fig_sel[n]);
			n++;
		}
	proyecta_snap_control_points();
}


/***********************************************/
// Nombre: deselect_fig                    
//															  
// Comentarios: borra la lista de seleccion 
// 
// Salida: ninguna 
/**********************************************/


void deselect_fig(void)
{
 	figura *fig1;
	GList *lista;
	float i;

	//borra la lista de figuras seleccionadas
 	lista=g_list_first(lista_seleccion);
	lista=g_list_next(lista);


	while (lista!=NULL)
		{
		   fig1=lista->data;	
			lista=g_list_remove(lista,fig1);
			fig1->selected=0;
			remove_control_snap_points(fig1);
		}
		
}

/***********************************************/
// Nombre:indica_figura_seleccionada                    
//															  
// Comentarios: indica a la figura que ha sido 
//					seleccionada
// 
// Salida: ninguna 
/**********************************************/



void indica_figura_seleccionada(figura *fig)
{
	int i;
	punto pto;	
	
	fig->selected=1;
	
	for(i=0;i<fig->num_parametros_generadores;i++)
		{
		pto=transforma_punto(fig->matriz_rotacion,fig->parametros_generadores[i]);
		pto=transforma_punto(fig->matriz_translacion,pto);
		add_snap_control_point(fig,pto,i);
		}

}

/***********************************************/
// Nombre: dibuja_seleccion                     
//															  
// Comentarios: dibuja el cuadro de seleccion
// 
// Salida: ninguna 
/**********************************************/
void dibuja_seleccion(void)
{
	glBegin(GL_LINE_STRIP);
	glVertex3f(screen_info.select_point_begin.x,screen_info.select_point_begin.y,0);
	glVertex3f(screen_info.select_point_end.x,screen_info.select_point_begin.y,0);
	glVertex3f(screen_info.select_point_end.x,screen_info.select_point_end.y,0);
	glVertex3f(screen_info.select_point_begin.x,screen_info.select_point_end.y,0);
	glVertex3f(screen_info.select_point_begin.x,screen_info.select_point_begin.y,0);
	glEnd();
}



/***********************************************/
// Nombre: dibuja_seleccion                     
//															  
// Comentarios: dibuja el cuadro de seleccion
// 
// Salida: ninguna 
/**********************************************/

int get_node_from_mouse(figura (*func)(),figura *fig,int com)
{
	static int button=0;
	punto transformado;
	SDL_Event event;
	SDL_PollEvent(&event);
	
	//comprueba la entrada
	switch (event.type)
		{
		case SDL_MOUSEBUTTONUP:

		//si se ha pulsado el boton del raton
		if(button==0)
			{
			//obliga a soltar el boton del raton antes de meter otro parametro
			button=1;
			return 1;
			}
		break;

		//en caso de movimiento
		case SDL_MOUSEMOTION:
			button=0;
			
			//inversa_tr=jccCalculaInversa(fig->matriz_translacion);
			transformado=request_point(event);
			get_snap_points(&transformado);
			
			fig->parametros_generadores[com]=transforma_punto(fig->matriz_translacion_i,transformado);
			fig->parametros_generadores[com]=transforma_punto(fig->matriz_rotacion_i,fig->parametros_generadores[com]);
			
			//para trazar perpendiculares
			snap_orto(fig->parametros_generadores[com-1],&fig->parametros_generadores[com]);
			
			jcc_aux_move(cursor,transformado);
			//refresca la salida		
			*fig=func(*fig,fig->parametros_generadores,fig->num_parametros_generadores);
			calcula_puntos_precalculados(fig,0,fig->longitud);
			SDL_Delay(1);
		break;
		}	
		
}



void set_parameter(figura *fig,int nodo)
{
	int com,leave,i;
	figura *fig1;
	int button_up=0;
	SDL_Event event;
	punto pto_mouse; 
	fig1=fig;

	
	do
		{
		leave=get_node_from_mouse(fig->func,fig,nodo);
  		}while(leave!=1);
	
	remove_control_snap_points(fig);
	remove_all_snap_points(fig);
	add_snaps(fig);
	indica_figura_seleccionada(fig);
	proyecta_snap_control_points();
	proyecta_snap_points();

}


