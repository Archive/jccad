/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
 
%{
#ifndef _PARSER_
#define _PARSER_
#include "parser.h"
#include "comandos.h"
#include "punto.h"
#endif
%}

YY_BUFFER_STATE resto[100];

	int numero,num_lineas = 0, num_caracteres = 0 ,num_coord=0;
%x p_punto
%x r_punto
%x parametro

%x dxf 

%x dxf_line
%x dxf_circle
%x dxf_elipse
%x dxf_arc
%x dxf_polyline
%x dxf_vertex

%x dxf_vertex_x
%x dxf_vertex_y
%x dxf_vertex_z


%x dxf_coor_x
%x dxf_coor_y
%x dxf_coor_z

%x dxf_center_x
%x dxf_center_y
%x dxf_center_z

%x dxf_elipse_center_x
%x dxf_elipse_center_y
%x dxf_elipse_center_z

%x dxf_radio
%x dxf_elipse_mayor
%x dxf_elipse_menor


TEXTO ([a-z/.]+)
ENTERO ([0-9]+)
ENTERO_N ("-"[0-9]+)
REAL ({ENTERO}|{ENTERO_N})"."{ENTERO}
PUNTO (({REAL}|{ENTERO}|{ENTERO_N})","({REAL}|{ENTERO}|{ENTERO_N})","({REAL}|{ENTERO}|{ENTERO_N}))
OPCION ([0-9]+)

%%
\n      ++num_caracteres;BEGIN(INITIAL);

<parametro>{TEXTO} {
							strcpy(shmptr->ret_msg,yytext);
						 }

<p_punto>{REAL}|{ENTERO}|{ENTERO_N}  { 	
								
								
								if(num_coord==0)
								{
								shmptr->parametros[shmptr->n_param_to_read].x=atof(yytext);
								shmptr->ultimo_parametro.x=atof(yytext);
								num_coord++;
								yylex();
								}
							if(num_coord==1)
								{
								shmptr->parametros[shmptr->n_param_to_read].y=atof(yytext);
								num_coord++;
								shmptr->ultimo_parametro.y=atof(yytext);
								yylex();
								}
							if(num_coord==2)
								{
								shmptr->parametros[shmptr->n_param_to_read].z=atof(yytext);
								shmptr->ultimo_parametro.z=atof(yytext);
								shmptr->n_param_to_read++;
								num_coord=0; 
								BEGIN(INITIAL);
								yylex();
								}
						}


<r_punto>{REAL}|{ENTERO}|{ENTERO_N}  { 	
								if(num_coord==0)
								{
								shmptr->parametros[shmptr->n_param_to_read].x=atof(yytext)+shmptr->ultimo_parametro.x;
								
								shmptr->ultimo_parametro.x=shmptr->parametros[shmptr->n_param_to_read].x;
								num_coord++;
								yylex();
								}
							if(num_coord==1)
									{
								shmptr->parametros[shmptr->n_param_to_read].y=atof(yytext)+shmptr->ultimo_parametro.y;
								shmptr->ultimo_parametro.y=shmptr->parametros[shmptr->n_param_to_read].y;
								num_coord++;
								yylex();
								}
							if(num_coord==2)
								{
								shmptr->parametros[shmptr->n_param_to_read].z=atof(yytext)+shmptr->ultimo_parametro.z;
								shmptr->ultimo_parametro.z=shmptr->parametros[shmptr->n_param_to_read].z;
								shmptr->n_param_to_read++;
								num_coord=0; 
								BEGIN(INITIAL);
								yylex();
								}
						}


lwidth" "{ENTERO}	{
		sscanf(yytext,"lwidth %d",&shmptr->lwidth);
		 }	 

ltype" "{ENTERO}	{
		sscanf(yytext,"ltype %d",&shmptr->linetype);
		 }	

save   {
			shmptr->comando=savef;
		 	BEGIN(parametro);
		 }

load   {
			shmptr->comando=loadf;
			BEGIN(parametro);
		 }	 

import_dxf {
			shmptr->comando=loaddxf;
			BEGIN(parametro);
		 }

{TEXTO} {
			jcc_comandos_exec(yytext);
		  }

{PUNTO}  {texto_a_GUI(yytext,0); 
			 BEGIN(p_punto); 
			 yyless(0);
			}

@{PUNTO}  {texto_a_GUI(yytext,0); 
			 BEGIN(r_punto); 
	       yyless(0);
			}

{OPCION} {texto_a_GUI(yytext,0); 
				shmptr->options[0]=atoi(yytext);
				shmptr->n_opts_to_read++;
			}

<dxf><*>.|\n ECHO;




<dxf>^LINE {
				BEGIN(dxf_line);
				}

<dxf>^CIRCLE {
				BEGIN(dxf_circle);
				}

<dxf>^POLYLINE {
				BEGIN(dxf_polyline);
				}



<dxf_polyline>VERTEX {
				 BEGIN(dxf_vertex);
				 }


<dxf_vertex>10 {
				 BEGIN(dxf_vertex_x);
				 }
<dxf_vertex>20 {
				 BEGIN(dxf_vertex_y);
				 }
<dxf_vertex>30 {
				 BEGIN(dxf_vertex_z);
				 }


<dxf_vertex_x>{REAL}|{ENTERO}|{ENTERO_N} {

						shmptr->parametros[shmptr->n_param_to_read].x=atof(yytext);
						BEGIN(dxf_vertex);
						}

<dxf_vertex_y>{REAL}|{ENTERO}|{ENTERO_N} {
				 		shmptr->parametros[shmptr->n_param_to_read].y=atof(yytext);
						BEGIN(dxf_vertex);
						}

<dxf_vertex_z>{REAL}|{ENTERO}|{ENTERO_N} {
				 		shmptr->parametros[shmptr->n_param_to_read].z=atof(yytext);
						shmptr->n_param_to_read++;
						BEGIN(dxf_polyline);
						}

<dxf_polyline>SEQEND {
						shmptr->end_figure=shmptr->n_param_to_read;
						jcc_comandos_exec("pline");
						jcc_comandos_get();
						BEGIN(dxf);
						}




<dxf_line>10|11 {
				 BEGIN(dxf_coor_x);
				 }

<dxf_line>20|21 {
				 BEGIN(dxf_coor_y);
				 }

<dxf_line>30|31 {
				 BEGIN(dxf_coor_z);
				 }


<dxf_circle>10 {
				 BEGIN(dxf_center_x);
				 }

<dxf_circle>20 {
				 BEGIN(dxf_center_y);
				 }

<dxf_circle>30 {
				 BEGIN(dxf_center_z);
				 }

<dxf_circle>40 {
				 BEGIN(dxf_radio);
				 }



<dxf_coor_x>{REAL}|{ENTERO}|{ENTERO_N} {
				 		shmptr->parametros[shmptr->n_param_to_read].x=atof(yytext);
						BEGIN(dxf_line);
						}

<dxf_coor_y>{REAL}|{ENTERO}|{ENTERO_N} {
							shmptr->parametros[shmptr->n_param_to_read].y=atof(yytext);
							BEGIN(dxf_line);
							}

<dxf_coor_z>{REAL} {
							shmptr->parametros[shmptr->n_param_to_read].z=atof(yytext);
							shmptr->n_param_to_read++;
						
							if(shmptr->n_param_to_read>1)
								{
								jcc_comandos_exec("line");
								jcc_comandos_get();
								BEGIN(dxf);
							}else BEGIN(dxf_line);
						 }

<dxf_center_x>{REAL}|{ENTERO}|{ENTERO_N} {
				 		shmptr->parametros[shmptr->n_param_to_read].x=atof(yytext);
						BEGIN(dxf_circle);
						}

<dxf_center_y>{REAL}|{ENTERO}|{ENTERO_N} {
							shmptr->parametros[shmptr->n_param_to_read].y=atof(yytext);
							BEGIN(dxf_circle);
							}

<dxf_center_z>{REAL} {
							shmptr->parametros[shmptr->n_param_to_read].z=atof(yytext);
							shmptr->n_param_to_read++;
							BEGIN(dxf_circle);
							}

<dxf_radio>{REAL} {
							shmptr->parametros[shmptr->n_param_to_read].x=shmptr->parametros[shmptr->n_param_to_read-1].x+atof(yytext);
				 			shmptr->parametros[shmptr->n_param_to_read].y=shmptr->parametros[shmptr->n_param_to_read-1].y;
							shmptr->parametros[shmptr->n_param_to_read].z=shmptr->parametros[shmptr->n_param_to_read-1].z;
							
							shmptr->n_param_to_read++;
							jcc_comandos_exec("circle");
							jcc_comandos_get();
							BEGIN(dxf);
						}

<<EOF>> {
			shmptr->mouse_enabled=0;
			BEGIN(INITIAL); 
			return 0;}


%%



void parse_text(char *texto)
        {
		  yy_scan_string(texto);
		  yylex();
        }

void dxf_load_file(char *texto)
        {
		  shmptr->mouse_enabled=1;
		  BEGIN(dxf);
		  yyin = fopen(texto, "r" );
		  if(yyin!=NULL) 
		  		yy_switch_to_buffer(yy_create_buffer( yyin, YY_BUF_SIZE ) );
		  yylex();
        }


