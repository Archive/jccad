/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#ifndef _ELIPSE_H
#define _ELIPSE_H

#include <glib.h>
#include "metodos.h"
#include "math.h"
#include <dlfcn.h>


figura eqelipse(figura fig,punto *ptos,int orden)
{
	double eje_x,eje_y,radio;
	
	
	eje_x=ptos[1].x-ptos[0].x;
	eje_y=ptos[1].y-ptos[0].y;

	
	if(orden==0)
		{
			eje_x=0;
			eje_y=0;
		}
	
	if(!eje_x){eje_x=0;}
	if(!eje_y){eje_y=0;}
	
	fig.orden=1;
	fig.base_fx.b_monomio[0]=ptos[0].x;
	fig.base_fy.b_monomio[0]=ptos[0].y;
	fig.base_fz.b_monomio[0]=ptos[0].z;

	fig.base_fy.seno[1]=eje_y;
	fig.base_fx.coseno[1]=eje_x;
	fig.centro=ptos[0];
	
	return fig;
}


void new_elipse(void)
{
	int com;
	figura *fig;
	
	fig=crea_figura();
	lista_figuras=g_list_append(lista_figuras,fig);
	
	fig->parametros_generadores=malloc(sizeof(punto)*4);

	//dando color
	fig->color[0]=shmptr->color_r;
	fig->color[1]=shmptr->color_g;
	fig->color[2]=shmptr->color_b;
		
	fig->correc=1;
	fig->n_nodo=8;
	fig->s_nodo=0;
	fig->e_nodo=8;

	fig->func=eqelipse;
//	fig->TIPO=elipse;
	sprintf(fig->func_name,"eqelipse");
	
	do{
	//comprueba el numero de parametros recogidos en fig
	com=fig->num_parametros_generadores;
	switch(com)
		{
		case 0:
		texto_a_GUI("Input center point",NULL);
		break;
		case 1:
		texto_a_GUI("Input second point",NULL);
		//cogemos los parametros introducidos desde la linea de comandos
		break;
		}
		get_parameters_from_input(fig->func,com,fig);
		//y tambien desde el raton
		get_parameters_from_mouse(fig->func,fig,com);
	}while(com<=1);


	shmptr->n_param_to_read=0;
	
	//SNAP CENTRO
	add_snap_center_point(fig);
	
	add_snaps(fig);
	
	return fig;
}

#endif
