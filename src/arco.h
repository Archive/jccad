/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
 
#ifndef _ARCO_H
#define _ARCO_H

#include <glib.h>
#include "metodos.h"
#include "figura.h"
#include "math.h"
#include "SDL.h"


figura p_arco(figura fig,punto *ptos,int orden)
{
	int i,j,k;
	double a,b,c;

	fig.orden=2;
	fig.longitud=2;
	fig.correc=1;
	fig.n_nodo=3;
	fig.s_nodo=0;
	fig.e_nodo=3;


	c=(ptos[2].x-2*ptos[1].x+ptos[0].x)/2;
	a=ptos[0].x;
	b=(ptos[1].x)-c-a;

	fig.base_fx.b_monomio[0]=a;
	fig.base_fx.b_monomio[1]=b;
	fig.base_fx.b_monomio[2]=c;

	c=(ptos[2].y-2*ptos[1].y+ptos[0].y)/2;
	a=ptos[0].y;
	b=(ptos[1].y)-c-a;


	fig.base_fy.b_monomio[0]=a;
	fig.base_fy.b_monomio[1]=b;
	fig.base_fy.b_monomio[2]=c;
	
	return fig;
}	



void new_arco(void)
{
	int com;
	int button;
	figura *fig;
	
	fig=crea_figura();
	lista_figuras=g_list_append(lista_figuras,fig);
	
	fig->parametros_generadores=malloc(sizeof(punto)*4);
	fig->func=p_arco;
//	fig->TIPO=arco;
	sprintf(fig->func_name,"p_arco");	
	
	//dando color
	fig->color[0]=shmptr->color_r;
	fig->color[1]=shmptr->color_g;
	fig->color[2]=shmptr->color_b;
					

	do
	{
		//comprueba el numero de parametros recogidos en fig
		com=fig->num_parametros_generadores;
		texto_a_GUI("Introduzca punto %d de la linea",com+1);
		//cogemos los parametros introducidos desde la linea de comandos
		get_parameters_from_input(fig->func,com,fig);
		//y tambien desde el raton
		get_parameters_from_mouse(fig->func,fig,com);
	}while(com<=2);
													
	shmptr->comando=0;	
	
	shmptr->n_param_to_read=0;

	add_snaps(fig);

}
#endif
