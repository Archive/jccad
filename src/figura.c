/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#include "math.h"
#include "comandos.h"
#include "punto.h"
#include <gnome.h> 
#include "figura.h"
#include "matriz_op.h"

/***********************************************
	Function: jcc_figura_new                     
**********************************************/

figura *crea_figura(void)
{
	figura *fig;
	fig=malloc(sizeof(figura));
	memset(fig,0,sizeof(figura));

	fig->ID=fig;
	fig->TIPO=0;
	fig->capa=0;//capa donde se encuentra
	fig->can_be_selected=1;
	fig->grosor=0.1;
	fig->n_nodo=0;
	fig->s_nodo=0;
	fig->e_nodo=0;
	fig->longitud=2*M_PI;
	fig->correc=1;
	fig->num_parametros_generadores=0;
	fig->patron=shmptr->linetype;
	fig->metodo_generador=0;
	fig->num_puntos_precalculados=-1;
	fig->bloques=g_list_alloc();

	if(shmptr->lwidth) 
		{
		fig->grosor=shmptr->lwidth;
		}
		else fig->grosor=1;
	
	asocia_matriz_identidad(fig->matriz_translacion);
	asocia_matriz_identidad(fig->matriz_escala);
	asocia_matriz_identidad(fig->matriz_rotacion);
	asocia_matriz_identidad(fig->matriz_translacion_i);
	asocia_matriz_identidad(fig->matriz_escala_i);
	asocia_matriz_identidad(fig->matriz_rotacion_i);


//	copia_matriz(screen_info.URM_tr,fig->t_matriz_tr);
	return fig;
}

/***********************************************/
// Nombre: destruye_figura                     
//															  
// Comentarios: destruye una figura 
// 
// Salida: ninguna 
/**********************************************/


void destruye_figura(figura *fig)
{
	
/*	free(fig->base_fx.cubic);
	free(fig->base_fy.cubic);
	free(fig->base_fz.cubic);
*/
	g_list_free(fig->bloques);
	remove_control_snap_points(fig);
	remove_all_snap_points(fig);
	free(fig->parametros_generadores);
	free(fig->puntos_precalculados);
	free(fig);
}

/***********************************************/
// Nombre: calcular_puntos_precalculados                     
//															  
// Comentarios: calcula puntos de la figura 
// 
// Salida: ninguna 
/**********************************************/

void calcula_puntos_precalculados(figura *fig,double t_i,double t_f)
{
	//declaracion de variables
	double t;//parametro 
	punto pto;
	int i,j,inicio,fin,numero_puntos;
	int CPZ;//correccion por zoom
	double interv;
	punto *puntos;
	CPZ=1+(int)((10+(1))*fig->correc);
	
	inicio=(int)fig->s_nodo*CPZ;
	fin=(int)fig->e_nodo*CPZ+1;

	numero_puntos=fin-inicio;
	
	puntos=malloc((numero_puntos+1)*sizeof(punto));
	interv=t_f-t_i;

	for (i = inicio; i <= fin; i++)
	{
	   t=t_i+((interv)*i/fig->n_nodo/CPZ);
		puntos[i-inicio]=calcula_punto(t,fig);
	}
	
	fig->num_puntos_precalculados=-1;
	free(fig->puntos_precalculados);
	fig->puntos_precalculados=puntos;
	fig->num_puntos_precalculados=numero_puntos;
}

/***********************************************/
// Nombre:calcular_interseccion                      
//															  
// Comentarios: calcula interseccion entre 2 figuras 
// 
// Salida: 1 en caso de interseccion 
/**********************************************/

int calcular_interseccion(figura *fig1,figura *fig2)
{
	punto v_eje1,v_eje2,v3,centro_nb,transformed;
	double menor[4],determinante,cb_matrix[4];
	int i,j;
	double x,y,t1,t2;
	punto interseccion;
	int found_intersec=0;

//g_print("\nintersectando\n");

	if(fig1->x_max<=fig2->x_min||fig1->x_min>=fig2->x_max)
		return 0;
	
	if(fig1->y_max<=fig2->y_min||fig1->y_min>=fig2->y_max)
		return 0;
	
	if(fig1->z_max!=fig2->z_max)
		return 0;
	
	for(j=0;j<fig2->num_puntos_precalculados-1;j++)
	{
		for(i=0;i<fig1->num_puntos_precalculados-1;i++)
		{
		centro_nb=fig1->puntos_precalculados[i];
		v_eje1=resta_vectorial(fig2->puntos_precalculados[j+1],centro_nb);
		v_eje2=resta_vectorial(fig2->puntos_precalculados[j],centro_nb);
		v3=resta_vectorial(fig1->puntos_precalculados[i+1],centro_nb);

		if(((v_eje1.x*v_eje2.y)-(v_eje1.y*v_eje2.x))==0)
		{
			centro_nb=fig1->puntos_precalculados[i+1];
			v_eje1=resta_vectorial(fig2->puntos_precalculados[j+1],centro_nb);
			v_eje2=resta_vectorial(fig2->puntos_precalculados[j],centro_nb);
			v3=resta_vectorial(fig1->puntos_precalculados[i],centro_nb);
		}

		menor[0]=v_eje2.y;
		menor[1]=v_eje1.y;
		menor[2]=v_eje2.x;
		menor[3]=v_eje1.x;

		determinante=(menor[3]*menor[0])-(menor[1]*menor[2]);

		cb_matrix[0]=menor[0]/determinante;
		cb_matrix[1]=-menor[2]/determinante;
		cb_matrix[2]=-menor[1]/determinante;
		cb_matrix[3]=menor[3]/determinante;

		transformed.x=cb_matrix[0]*v3.x+cb_matrix[1]*v3.y;
		transformed.y=cb_matrix[2]*v3.x+cb_matrix[3]*v3.y;

		if(transformed.x+transformed.y>=1&&transformed.x>=0&&transformed.y>=0)
		{
			if((transformed.x+transformed.y)!=0)
			{
			x=(transformed.x/(transformed.x+transformed.y));
			y=1-x;
			}else
			{
				x=0;
				y=0;
			}
			interseccion.x=(v_eje1.x*x)+(v_eje2.x*y)+centro_nb.x;
			interseccion.y=(v_eje1.y*x)+(v_eje2.y*y)+centro_nb.y;
			interseccion.z=0;

			(double)t1=(((double)(i+(x/transformed.x))/
						  (fig1->num_puntos_precalculados-1))*fig1->longitud);
			(double)t2=((double)(j)/fig2->num_puntos_precalculados)*fig2->longitud;


			add_snap_intersection(fig1,t1,fig2,t2,interseccion);
			found_intersec=1;
			}
		}
	}
return found_intersec;
}



/***********************************************/
// Nombre: calcula_punto                     
//															  
// Comentarios: calcula un punto de la figura
// 
// Salida: punto calculado 
/**********************************************/

punto calcula_punto(double t,figura *fig)
{
	punto pto;
	int orden,i,j,ti;
	double rt,rt_2,rt_sen,rt_cos,sen_t,cos_t,*matriz_tr;

	pto.x=0;
	pto.y=0;
	pto.z=0;
	pto.t=t;

	ti=(int)t;

	sen_t=sin(t);
	cos_t=cos(t);
	rt_2=pow(t,2);
	
	if(fig->is_spline==1)
	{
		fig->base_fy.b_monomio[0]=fig->base_fy.cubic[ti].b_monomios[0];
		fig->base_fy.b_monomio[1]=fig->base_fy.cubic[ti].b_monomios[1];
		fig->base_fy.b_monomio[2]=fig->base_fy.cubic[ti].b_monomios[2];
		fig->base_fy.b_monomio[3]=fig->base_fy.cubic[ti].b_monomios[3];


		fig->base_fx.b_monomio[0]=fig->base_fx.cubic[ti].b_monomios[0];
		fig->base_fx.b_monomio[1]=fig->base_fx.cubic[ti].b_monomios[1];
		fig->base_fx.b_monomio[2]=fig->base_fx.cubic[ti].b_monomios[2];
		fig->base_fx.b_monomio[3]=fig->base_fx.cubic[ti].b_monomios[3];

		fig->base_fz.b_monomio[0]=fig->base_fz.cubic[ti].b_monomios[0];
		fig->base_fz.b_monomio[1]=fig->base_fz.cubic[ti].b_monomios[1];
		fig->base_fz.b_monomio[2]=fig->base_fz.cubic[ti].b_monomios[2];
		fig->base_fz.b_monomio[3]=fig->base_fz.cubic[ti].b_monomios[3];
	}


	for(orden=0;orden<=fig->orden;orden++)
	{
		rt=pow(t,orden);
		rt_sen=pow(sen_t,orden);
		rt_cos=pow(cos_t,orden);
		//base de monomios
		pto.x+=fig->base_fx.b_monomio[orden]*rt;
		pto.y+=fig->base_fy.b_monomio[orden]*rt;
		pto.z+=fig->base_fz.b_monomio[orden]*rt;

		//base de senos
		pto.x+=fig->base_fx.seno[orden]*rt_sen;
		pto.y+=fig->base_fy.seno[orden]*rt_sen;
		pto.z+=fig->base_fz.seno[orden]*rt_sen;
		//base de cosenos
		pto.x+=fig->base_fx.coseno[orden]*rt_cos;
		pto.y+=fig->base_fy.coseno[orden]*rt_cos;
		pto.z+=fig->base_fz.coseno[orden]*rt_cos;
	}
	
	matriz_tr=fig->matriz_translacion;
	
	
	pto=transforma_punto(fig->matriz_escala,pto);
	pto=transforma_punto(fig->matriz_rotacion,pto);
	pto=transforma_punto(fig->matriz_translacion,pto);

	return pto;
}



int comprueba_figura(figura *fig,GList *lista)
{
	GList *lista_act;
	figura *fig1;
	
	lista_act=g_list_first(lista);
	lista_act=g_list_next(lista_act);

	while(lista_act !=NULL)
		{
			fig1=lista_act->data;
			
			if(fig1==fig)
			{
				return 1;
			}
			lista_act=g_list_next(lista_act);
			}
	return 0;
}



/***********************************************/
// Nombre: copia_figura                     
//															  
// Comentarios: copia una figura 
// 
// Salida: puntero a figura 
/**********************************************/

figura *copia_figura(figura *fig,GList *lista)
{
	figura *fig1;
	int i;
	
	fig1=crea_figura();
	memcpy(fig1,fig,sizeof(figura));
	fig1->ID=fig1;
	fig1->bloques=g_list_alloc();
	fig1->parametros_generadores=malloc(sizeof(punto)*(fig->num_parametros_generadores+1));
	fig1->puntos_precalculados=NULL;
	
	for(i=0;i<=fig->num_parametros_generadores;i++)
		{
			fig1->parametros_generadores[i]=fig->parametros_generadores[i];
		}	
	
	calcula_puntos_precalculados(fig1,fig1->t_inicial,fig1->longitud);
	lista=g_list_append(lista,fig1);	
	
	//g_print("\nfigura copiada-num_par %d\n",fig1->bloques);
	return fig1;
}	

/***********************************************/
// Nombre: translada_figura                     
//															  
// Comentarios: translada una figura 
// 
// Salida: ninguna 
/**********************************************/

void translada_figura(figura *fig,punto base,punto destino)
{
	double *inv;
	int i;	
	
	fig->matriz_translacion[3]+=destino.x-base.x;
	fig->matriz_translacion[7]+=destino.y-base.y;
	fig->matriz_translacion[11]+=destino.z-base.z;
	calcula_puntos_precalculados(fig,fig->t_inicial,fig->longitud);
	
	inv=jccCalculaInversa(fig->matriz_translacion);

	for(i=0;i<16;i++)
	{
	fig->matriz_translacion_i[i]=inv[i];
	}
	
	free(inv);
}

/***********************************************
	Function: jcc_entity_rotate                     
**********************************************/

void jcc_entity_rotate(figura *fig,punto base,double angle)
{
	double *inv,*rot_t,rot_m[16];
	int i;	
	punto centro,destino;
	
	asocia_matriz_identidad(rot_m);

	rot_m[0]=cos(angle);
	rot_m[1]=sin(angle);
	rot_m[4]=-sin(angle);
	rot_m[5]=cos(angle);
	
	centro.x=base.x-fig->matriz_translacion[3];
	centro.y=base.y-fig->matriz_translacion[7];

	destino=transforma_punto(rot_m,centro);
	
	rot_t=mul_matrix(fig->matriz_rotacion,rot_m);
	
	inv=jccCalculaInversa(rot_t);
	
	for(i=0;i<16;i++)
	{
		fig->matriz_rotacion[i]=rot_t[i];
		fig->matriz_rotacion_i[i]=inv[i];
	}
	
	calcula_puntos_precalculados(fig,fig->t_inicial,fig->longitud);
	
	translada_figura(fig,destino,centro);
	
	free(rot_t);
	free(inv);
}


/***********************************************/
// Nombre: escala_figura                     
//															  
// Comentarios: escala una figura 
// 
// Salida: ninguna 
/**********************************************/

void escala_figura(figura *fig,punto escala)
{
	double *inv;
	int i;	
	
	fig->matriz_translacion[0]+=escala.x;
	fig->matriz_translacion[5]+=escala.y;
	fig->matriz_translacion[10]+=escala.z;
	calcula_puntos_precalculados(fig,fig->t_inicial,fig->longitud);
	
	inv=jccCalculaInversa(fig->matriz_translacion);

	for(i=0;i<16;i++)
	{
	fig->matriz_translacion_i[i]=inv[i];
	}

}


