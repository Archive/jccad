/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#include <string.h>
#include <gtk/gtk.h>
#include <gnome.h> 
#include <glade/glade.h>
#include "comandos.h"
#include "glade_callbacks.h"

GtkWidget *label;

void actualiza_coordenadas(void)
{
	char msg[40];

	GtkTextBuffer *buffer;

	buffer=gtk_text_view_get_buffer(label);

	sprintf(msg,"Posicion\nX=%f\nY=%f\nZ=%f",shmptr->cursor.x,shmptr->cursor.y,shmptr->cursor.z);
	gtk_text_buffer_set_text(buffer,msg,40);

}

//declaracion de funciones privadas
int timeout_callback( gpointer data )
{
	static int i,ptimer;
	char msg[40];

	GtkTextBuffer *buffer1;

	buffer1=gtk_text_view_get_buffer(textview);
	
	gtk_timeout_remove( ptimer);
	ptimer=gtk_timeout_add(100,timeout_callback,data);

	actualiza_coordenadas();
	if(shmptr->is_msg==1)
	{
		gtk_text_buffer_set_text(buffer1,shmptr->ret_msg,strlen(shmptr->ret_msg));
		shmptr->is_msg=0;
	}
}



/***********************************************/
// Nombre: crea_GUI                     
//															  
// Comentarios: inicia el gui  
// 
// Salida: 0 en caso de error; 1 en otro caso 
/**********************************************/

void crea_gui(void)
{
 	 GladeXML *xml;
	 GtkWidget *button,*TextView;	
	 GtkTextBuffer *buffer;
	 GtkTextIter begin, end;
	 
	 xml = glade_xml_new (JCC_AUX_DIR"/gui.glade",NULL,NULL);
	 button = glade_xml_get_widget (xml, "button");
	 label = glade_xml_get_widget (xml, "cinfo");
	 colorpicker=glade_xml_get_widget(xml,"colorpicker1");
	 textview=glade_xml_get_widget(xml,"textview2"); 
 
	 //color inicial (blanco)
	 gnome_color_picker_set_i16 (colorpicker,65535,65535,65535,0);

	 shmptr->color_r=1;
	 shmptr->color_g=1;
	 shmptr->color_b=1;

	 //buffer de texto
	 buffer=gtk_text_view_get_buffer(label);
	 gtk_text_buffer_get_start_iter (buffer, &begin);
	 gtk_text_buffer_get_end_iter (buffer, &end);

 
	 glade_xml_signal_autoconnect(xml);

	 gtk_timeout_add(100,timeout_callback,label);
	 
	 sleep(0.5);
	 
	 return 1;
}


