/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
 
#include <gnome.h>
#include "listas.h"
#include "snap.h"
#include "listas.h"

#define SHM_SIZE    10000
#define SHM_MODE    (SHM_R | SHM_W)     /* read/write */

/***********************************************/
// Nombre: inicia_listas                     
//															  
// 
// Salida: 0 en caso de error; 1 en otro caso 
/**********************************************/

int inicia_listas(void)
{
	lista_seleccion=g_list_alloc();	
	lista_auxiliar=g_list_alloc();	
	lista_figuras=g_list_alloc();
	//listas para los snaps
	snap_points.intersection=g_list_alloc();
	snap_points.end_points=g_list_alloc();
	snap_points.mid_points=g_list_alloc();
	snap_points.control_points=g_list_alloc();
	snap_points.center_points=g_list_alloc();
	snap_points.s_points=g_list_alloc();

	return 1;
}



/***********************************************/
// Nombre: destruye_listas                     
//															  
// 
// Salida: 0 en caso de error; 1 en otro caso 
/**********************************************/

int libera_listas(void)
{
	g_list_free(lista_seleccion);
	g_list_free(lista_auxiliar);
	g_list_free(lista_figuras);
	//listas para los snaps
	g_list_free(snap_points.intersection);
	g_list_free(snap_points.end_points);
	g_list_free(snap_points.mid_points);
	g_list_free(snap_points.control_points);
	g_list_free(snap_points.center_points);
	g_list_free(snap_points.s_points);

	return 1;
}
