/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#include "screen.h"
#include "init.h"
#include "comandos.h"
#include "auxiliares.h"
#include "eventos.h"
#include "files.h"
#include "listas.h"
#include "matriz_op.h"
#include "figura.h"
#include <GL/gl.h>
#include <gnome.h>

SDL_TimerID timer;

//declaracion de funciones privadas
void redibujar(void);
void gl_init(void);
void libera_memoria(void);



void libera_memoria(void)
{
	figura *fig;
	GList *lista_act;
	
	lista_act=g_list_first(lista_figuras);
	lista_act=g_list_next(lista_act);

	while(lista_act !=NULL)
		{
			fig=lista_act->data;
			destruye_figura(fig);		
			lista_act=g_list_next(lista_act);
		}
	
	lista_act=g_list_first(lista_auxiliar);
	lista_act=g_list_next(lista_act);

	while(lista_act !=NULL)
		{
			fig=lista_act->data;
			destruye_figura(fig);		
			lista_act=g_list_next(lista_act);
		}
	
	libera_listas();
}



/***********************************************/
// Nombre: gl_init                     
//															  
// Comentarios: inicia las opciones OGL necesarias
// 
// Salida: ninguna 
/**********************************************/
void gl_init(void)
{
	screen_info.RES_X=700;
	screen_info.RES_Y=700;
	screen_info.zoom=200;
	screen_info.scroll_x=0;
	screen_info.scroll_y=0;

	screen = SDL_SetVideoMode(screen_info.RES_X, screen_info.RES_Y, 16, SDL_OPENGL);
	
	SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 5 );
	SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 5 );
	SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 5 );
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
	//Cleaning color
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_TEXTURE_1D);
	glEnable (GL_LINE_STIPPLE);
	glPushAttrib (GL_LINE_BIT);
	

	SDL_ShowCursor(SDL_DISABLE);

//	atexit(SDL_Quit);
}


/***********************************************/
// Nombre: redibuja                     
//															  
// Comentarios: funcion que redibuja la escena
// 
// Salida: ninguna 
/**********************************************/

void redibujar(void)
{
	int done=0;

	mut_dibujo=SDL_CreateMutex();
	init_lcad();
	gl_init();
	
	inicializado=1;
	
	do{
		SDL_mutexP(mut_dibujo);
		done = shmptr->quit;
		
		if(screen_info.select_state>=2)	
		{
			select_fig(NULL);	
			screen_info.select_state=0;
		}
		else
			jcc_draw(lista_figuras);
		
		SDL_mutexV(mut_dibujo);
		SDL_Delay(20);
	}while(done!=1);
	
}


/***********************************************/
// Nombre: init_lcad                     
//															  
// Comentarios: inicia lcad
// 
// Salida: 0 en caso de error; 1 en otro caso 
/**********************************************/

int init_lcad(void)
{
	//iniciando el timer
	
	if (SDL_Init(SDL_INIT_TIMER) < 0)
		{
		g_print("\n error al iniciar el timer");
		return 0;
		}
	
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		{
		g_print("\n error al iniciar el modo de video");
		return 1;
		}
	
	jcc_comandos_new();
	inicia_listas();
	
	
	SDL_WM_SetCaption("JCCad", "JCCad");

	ejes=jcc_aux_load(JCC_AUX_DIR "/ejes");	
	cursor=jcc_aux_load(JCC_AUX_DIR "/cursor");
	snap_aux=jcc_aux_load(JCC_AUX_DIR "/iasnap");
	control_aux=jcc_aux_load(JCC_AUX_DIR "/punto_control");
	
	g_print("\n inicializado");
}


/***********************************************/
// Nombre: lcad_main                     
//															  
// Comentarios: bucle principal de lcad
// 
// Salida: 1 en caso de error; 0 en otro caso 
/**********************************************/

int lcad_main(void)
{
	int done;
	SDL_Thread *thread;
	
	thread=SDL_CreateThread((void *)redibujar,NULL);	
	
	do{SDL_Delay(0.1);/*g_print("init->%d",inicializado);*/}while(inicializado!=1);
	while (done!=1)
		{
		if(shmptr->mouse_enabled==1) continue;
		done = shmptr->quit;
		
		jcc_comandos_get();
		comprobar_eventos();
		
		SDL_Delay(0.1);
		}
	SDL_KillThread(thread);
	SDL_Delay(20);
	libera_memoria();
	return 0;
}

void desactiva_dibujo(void)
{
	if(SDL_mutexP(mut_dibujo)==-1)
	{
  		fprintf(stderr, "Couldn't lock mutex\n");
    	exit(-1);
	}
}

void activa_dibujo(void)
{
	if(SDL_mutexV(mut_dibujo)==-1)
	{
  		fprintf(stderr, "Couldn't unlock mutex\n");
    	exit(-1);
	}
}
