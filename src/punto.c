/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
 
#include "punto.h"
#include "screen.h"
#include "GL/gl.h"
#include <gnome.h> 
#include "comandos.h"
#include "auxiliares.h"
#include "matriz_op.h"

/***********************************************/
// Nombre: calcula_ppoint                     
//															  
// Comentarios: proyecta un punto sobre el plano de  
//		dibujo
// 
// Salida: puntero a figura 
/**********************************************/

punto calcula_ppoint(punto pto)
{
	punto puntoa,puntob,normal1,puntof;
	double l;
	double URM[16];
	int i=0;
	

	normal1.x=screen_info.proyeccion[2];
	normal1.y=screen_info.proyeccion[6];
	normal1.z=screen_info.proyeccion[10];
/*

	puntoa.x=screen_info.URM_tr[0]+screen_info.URM_tr[3];
	puntoa.y=screen_info.URM_tr[4]+screen_info.URM_tr[7];
	puntoa.z=screen_info.URM_tr[8]+screen_info.URM_tr[11];

	puntob.x=screen_info.URM_tr[1]+screen_info.URM_tr[3];
	puntob.y=screen_info.URM_tr[5]+screen_info.URM_tr[7];
	puntob.z=screen_info.URM_tr[9]+screen_info.URM_tr[11];
*/	/*
l=-( pto.x*((puntoa.y*puntob.z)-(puntoa.z*puntob.y)) + 
	  pto.y*((puntoa.z*puntob.x)-(puntoa.x*puntob.z)) +
	  pto.z*((puntoa.x*puntob.y)-(puntoa.y*puntob.x)))/ 
 	( normal1.x*((puntoa.y*puntob.z)-(puntoa.x*puntob.y)) + 
	  normal1.y*((puntoa.z*puntob.x)-(puntoa.x*puntob.z)) +
	  normal1.z*((puntoa.x*puntob.y)-(puntoa.y*puntob.x)));
*/
//l=(pto.z-pto.y)/(normal1.y-normal1.z);

	l=-pto.z/normal1.z;


	normal1.x=normal1.x*l;
	normal1.y=normal1.y*l;
//	normal1.z=-pto.z;/*normal1.z*l;*/

	puntof.x=(pto.x+normal1.x);
	puntof.y=(pto.y+normal1.y);
	puntof.z=0;//pto.z+normal1.z;

	//puntof.z=-puntof.y+puntof.z;
	return puntof;
}

/***********************************************/
// Nombre: request_point                     
//															  
// Comentarios: devuelve el punto de la escena 
//		correspondiente al de la pantalla
// 
// Salida: punto 
/**********************************************/

punto request_point(SDL_Event event)
{
	punto position,puntoa,puntof;
	float *matriz_c;

	if(screen_info.drawing!=0)
		{return shmptr->cursor;}
/*
	position.x=
			  ((((float)(event.motion.x-(screen_info.RES_X/2))/
			  (screen_info.RES_X/2))*screen_info.zoom)
			  +(screen_info.scroll_x));

	position.y=
			  -((((float)(event.motion.y-(screen_info.RES_Y/2))/
			  (screen_info.RES_Y/2))*screen_info.zoom)
			  -(screen_info.scroll_y));
*/
	
	position.x=
			  ((((float)(event.motion.x-(screen_info.RES_X/2))/
			  (screen_info.RES_X/2))*screen_info.zoom)
			  +(screen_info.scroll_x));

	position.y=
			  -((((float)(event.motion.y-(screen_info.RES_Y/2))/
			  (screen_info.RES_Y/2))*screen_info.zoom)
			  -(screen_info.scroll_y));
	
	position.z=0;

	

/*	
	puntoa=transforma_punto((le*)screen_info.proyeccion,position);
*/	
	
	puntoa.x=(screen_info.proyeccion[0]*position.x)+(screen_info.proyeccion[1]*position.y)+(screen_info.proyeccion[2]*position.z)+screen_info.proyeccion[3];
	puntoa.y=(screen_info.proyeccion[4]*position.x)+(screen_info.proyeccion[5]*position.y)+(screen_info.proyeccion[6]*position.z)+screen_info.proyeccion[7];
	puntoa.z=(screen_info.proyeccion[8]*position.x)+(screen_info.proyeccion[9]*position.y)+(screen_info.proyeccion[10]*position.z)+screen_info.proyeccion[11];


	puntoa.x*=screen_info.zoom;
	puntoa.y*=screen_info.zoom;
	puntoa.z*=screen_info.zoom;

	puntof=calcula_ppoint(puntoa);

	shmptr->cursor=puntof;

	return puntof;
}

/***********************************************/
// Nombre: request_screen_point                     
//															  
// Comentarios: devuelve el punto de la escena 
//		correspondiente al de la pantalla
// 
// Salida: punto
/**********************************************/

punto request_screen_point(SDL_Event event)
{
	punto position;

	position.x=
			  ((((float)(event.motion.x-screen_info.RES_X/2)/(screen_info.RES_X/2))*screen_info.zoom));

	position.y=
			  -((((float)(event.motion.y-screen_info.RES_Y/2)/(screen_info.RES_Y/2))*screen_info.zoom));

	position.z=0;

	return position;
}
