/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#ifndef _POLILINEA_H
#define _POLILINEA_H

#include <glib.h>
#include "metodos.h"
#include "math.h"
#include "SDL.h"


figura eqpolilinea(figura fig,punto *ptos,int orden)
{
int i,j,k;
double a,b,c,d,l,m,t1,t2;

//orden=fig.e_nodo;

fig.orden=1;
fig.longitud=orden;
fig.n_nodo=orden;
fig.s_nodo=0;
fig.is_spline=1;

//calculamos cada tramo

//primero para x(t) y luego para y(t)

for(i=0;i<=orden;i++)
{
l=ptos[i].x;
m=ptos[i+1].x;

//calculamos los coeficientes
b=(m-l);
a=l-(b*i);

fig.base_fx.cubic[i].b_monomios[0]=a;
fig.base_fx.cubic[i].b_monomios[1]=b;
fig.base_fx.cubic[i].b_monomios[2]=0;
fig.base_fx.cubic[i].b_monomios[3]=0;

}

for(i=0;i<=orden;i++)
{
l=ptos[i].y;
m=ptos[i+1].y;

//calculamos los coeficientes
b=(m-l);
a=l-(b*i);

fig.base_fy.cubic[i].b_monomios[0]=a;
fig.base_fy.cubic[i].b_monomios[1]=b;
fig.base_fy.cubic[i].b_monomios[2]=0;
fig.base_fy.cubic[i].b_monomios[3]=0;
}

for(i=0;i<=orden;i++)
{
l=ptos[i].z;
m=ptos[i+1].z;

//calculamos los coeficientes
b=(m-l);
a=l-(b*i);

fig.base_fz.cubic[i].b_monomios[0]=a;
fig.base_fz.cubic[i].b_monomios[1]=b;
fig.base_fz.cubic[i].b_monomios[2]=0;
fig.base_fz.cubic[i].b_monomios[3]=0;

}

return fig;
}

void new_pline(void)
{
	int com=0;
	Uint8 *keys;
	figura *fig;
	
	fig=crea_figura();
	lista_figuras=g_list_append(lista_figuras,fig);
	


	fig->correc=0;

	fig->color[0]=shmptr->color_r;
	fig->color[1]=shmptr->color_g;
	fig->color[2]=shmptr->color_b;
	
	fig->parametros_generadores=malloc(sizeof(punto));
	fig->base_fx.cubic=malloc(sizeof(cubic_spl));
	fig->base_fy.cubic=malloc(sizeof(cubic_spl));
	fig->base_fz.cubic=malloc(sizeof(cubic_spl));

	fig->func=eqpolilinea;
	sprintf(fig->func_name,"eqpolilinea");
//	fig->TIPO=polilinea;

	do{

	
	keys = SDL_GetKeyState(NULL);
	
	//reservamos memoria suficiente para guardar los parametros
	fig->base_fx.cubic=realloc(fig->base_fx.cubic,(shmptr->end_figure+2+com)*sizeof(cubic_spl));
	fig->base_fy.cubic=realloc(fig->base_fy.cubic,(shmptr->end_figure+2+com)*sizeof(cubic_spl));
	fig->base_fz.cubic=realloc(fig->base_fz.cubic,(shmptr->end_figure+2+com)*sizeof(cubic_spl));

	fig->e_nodo=com;
	get_parameters_from_mouse(fig->func,fig,com);
	get_parameters_from_input(fig->func,com,fig);
	com=fig->num_parametros_generadores;

	texto_a_GUI("Input point %d",com+1);
	if(keys[SDLK_ESCAPE])shmptr->end_figure=1;
	}while(shmptr->end_figure<1);
	
	com--;
	*fig=fig->func(*fig,fig->parametros_generadores,com);
	fig->e_nodo=com;
	calcula_puntos_precalculados(fig,0,fig->longitud);
	add_snaps(fig);
	return fig;
}
#endif
