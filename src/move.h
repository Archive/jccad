/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
 
#ifndef _MOVE_H
#define _MOVE_H

#include <glib.h>
#include "metodos.h"
#include "math.h"
#include "figura.h"
#include "linea.h"
#include "listas.h"
#include "SDL.h"


figura *new_move(void)
{
	GList *lista;
	figura *fig,*fig1;
	int com;
	
	fig=crea_figura();
	
	fig->parametros_generadores=malloc(sizeof(punto)*3);
	
//	fig->TIPO=linea;
	fig->func=eqlinea;
	
g_print("moviendo");
	do{
//comprueba el numero de parametros recogidos en fig
	com=fig->num_parametros_generadores;
	
	switch(com)
		{
			case 0:
			texto_a_GUI("Introduzca origen",NULL);
			break;
			
			case 1:
			texto_a_GUI("Introduzca punto de destino ",NULL);
			break;
			
		}
		//cogemos los parametros introducidos desde la linea de comandos
		get_parameters_from_input(fig->func,com,fig);
		//y tambien desde el raton
		get_parameters_from_mouse(fig->func,fig,com);
		//2 puntos
	}while(com<=1);

	fig->num_parametros_generadores--;

	
	lista=g_list_first(lista_seleccion);
	lista=g_list_next(lista);
	
	while(lista!=NULL)
		{
		fig1=lista->data;
		translada_figura(fig1,fig->parametros_generadores[0],fig->parametros_generadores[1]);
		lista=g_list_next(lista);
		}
	
	lista=g_list_first(lista_seleccion);
	lista=g_list_next(lista);
	
	while(lista!=NULL)
		{
		remove_all_snap_points(fig1);
		add_snaps(fig1);
		lista=g_list_next(lista);
		}

	texto_a_GUI("Introduzce nuevo comando",com+1);
	shmptr->comando=0;	
	shmptr->n_param_to_read=0;

	

	return fig;
}
#endif
