/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
 
#ifndef _POLIGONO_H
#define _POLIGONO_H

#include <glib.h>
#include "metodos.h"
#include "math.h"
#include "SDL.h"

figura poligon(figura fig,punto *ptos,int orden)
{
int i,j,k;
double ang,init_ang,a,b,l,m,u,v,eje_x,eje_y,radio;


orden=fig.opciones_generadoras[0];

fig.orden=1;
fig.longitud=orden;
fig.s_nodo=0;
fig.e_nodo=orden;
fig.is_spline=1;


ang=(2*M_PI/orden);

eje_x=ptos[1].x-ptos[0].x;
eje_y=ptos[1].y-ptos[0].y;

if(!eje_x){eje_x=0;}
if(!eje_y){eje_y=0;}

radio=sqrt(pow(eje_x,2)+pow(eje_y,2));

init_ang=acos(eje_x/radio)*(eje_y/fabs(eje_y));
//calculamos cada tramo

//primero para x(t) y luego para y(t)

for(i=0;i<=orden;i++)
{
//calculo de nodos

l=(radio*cos((ang*i)+init_ang))+ptos[0].x;
m=(radio*cos((ang*(i+1))+init_ang))+ptos[0].x;
u=(radio*sin((ang*i)+init_ang))+ptos[0].y;
v=(radio*sin((ang*(i+1))+init_ang))+ptos[0].y;


//calculamos los coeficientes
b=(m-l);
a=l-(b*i);

fig.base_fx.cubic[i].b_monomios[0]=a;
fig.base_fx.cubic[i].b_monomios[1]=b;

b=(v-u);
a=u-(b*i);

fig.base_fy.cubic[i].b_monomios[0]=a;
fig.base_fy.cubic[i].b_monomios[1]=b;

fig.base_fz.cubic[i].b_monomios[0]=0;
fig.base_fz.cubic[i].b_monomios[1]=0;
}

return fig;
}

void new_poligon(void)
{
	int com,n_lados;
	figura *fig;
	
	fig=crea_figura();
	lista_figuras=g_list_append(lista_figuras,fig);

	fig->correc=0;

	fig->color[0]=shmptr->color_r;
	fig->color[1]=shmptr->color_g;
	fig->color[2]=shmptr->color_b;
	
do{
	com=fig->num_opciones_generadoras;
	texto_a_GUI("Introduzce numero de lados",NULL);
	get_options_from_input(com,fig);
	fig->parametros_generadores=realloc(fig->parametros_generadores,(com+2)*sizeof(punto)*5);
	}while(com==0);

	n_lados=fig->opciones_generadoras[0];

	fig->parametros_generadores=malloc(sizeof(punto)*(n_lados+1));
	fig->base_fx.cubic=malloc(sizeof(cubic_spl)*(n_lados+1));
	fig->base_fy.cubic=malloc(sizeof(cubic_spl)*(n_lados+1));
	fig->base_fz.cubic=malloc(sizeof(cubic_spl)*(n_lados+1));

	fig->func=poligon;
	fig->correc=0;
	sprintf(fig->func_name,"poligon");
//	fig->TIPO=poligono;
	
	fig->correc=0;
	fig->n_nodo=n_lados;
	fig->s_nodo=0;
	fig->e_nodo=n_lados;
	
	
	do{
	//comprueba el numero de parametros recogidos en fig
	com=fig->num_parametros_generadores;
	switch(com)
		{
		case 0:
		texto_a_GUI("Introduzca centro del poligono",NULL);
		break;
		case 1:
		texto_a_GUI("Introduzca punto del poligono",com);
		break;
		}
	//cogemos los parametros introducidos desde la linea de comandos
	get_parameters_from_input(fig->func,com,fig);
	//y tambien desde el raton
	get_parameters_from_mouse(fig->func,fig,com);
	}while(com<=1);
	
	*fig=fig->func(*fig,fig->parametros_generadores,com-1);
	calcula_puntos_precalculados(fig,0,fig->longitud);
	fig->num_parametros_generadores--;
	texto_a_GUI("Introduzce nuevo comando",com+1);
	add_snaps(fig);
	return fig;
}
#endif
