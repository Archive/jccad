/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
 
#ifndef _POINT_H
#define _POINT_H

#include <glib.h>
#include "math.h"


//la siguiente funcion asocia una linea a la figura
figura eqpunto(figura fig,punto *ptos,int orden)
{
	
	fig.base_fx.b_monomio[0]=ptos[0].x;
	fig.base_fy.b_monomio[0]=ptos[0].y;
	fig.base_fz.b_monomio[0]=ptos[0].z;
	
	if(orden>0)
		{
		remove_s_snap_points(&fig);
		add_snap_s_point(&fig,fig.puntos_precalculados[0]);
		jcc_aux_move(fig.aux_fig,fig.puntos_precalculados[0]);
		}

	return fig;
}

void new_point(void)
{
	int com;
	figura *fig;
	
	fig=crea_figura();
	lista_figuras=g_list_append(lista_figuras,fig);

	fig->parametros_generadores=malloc(sizeof(punto));
	fig->func=eqpunto;

	fig->color[0]=shmptr->color_r;
	fig->color[1]=shmptr->color_g;
	fig->color[2]=shmptr->color_b;

	fig->correc=0;
	fig->n_nodo=1;
	fig->s_nodo=0;
	fig->e_nodo=1;

//	fig->TIPO=spoint;
	fig->num_parametros_generadores=0;
	fig->func=eqpunto;
	
	fig->aux_fig=jcc_aux_load(JCC_AUX_DIR"/punto");
	
	do{
		//comprueba el numero de parametros recogidos en fig
		com=fig->num_parametros_generadores;
		texto_a_GUI("Input point %d ",com+1);
		//y tambien desde el raton
		get_parameters_from_mouse(fig->func,fig,com);
		//2 puntos
		}while(com<1);

		jcc_aux_move(fig->aux_fig,fig->puntos_precalculados[0]);
		add_snap_s_point(fig,fig->puntos_precalculados[0]);
//		fig->num_parametros_generadores--;
		return fig;
}

#endif
