/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#include <gnome.h> 
#include "gui.h"
#include "init.h"
#include "listas.h"
#include <SDL.h>
#include <SDL_thread.h>

#ifndef M_PI
# define M_PI		3.14159265358979323846	/* pi */
#endif


int main(int argc, char **argv)
{
	int pid;
	GThread *hilo;
	

	#ifdef ENABLE_NLS
 	bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
   bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
   textdomain (GETTEXT_PACKAGE);
	#endif

  	gnome_program_init ("Etsincad", "1.0", LIBGNOMEUI_MODULE,
                      argc,argv,
                      GNOME_PARAM_APP_DATADIR, PACKAGE_DATA_DIR,
                      NULL);

	SDL_CreateThread(lcad_main,NULL);	
	do{SDL_Delay(0.1);/*g_print("init->%d",inicializado);*/}while(inicializado!=1);	
	
	lcad_load_file(argv[1],lista_figuras);
	
	crea_gui();	
	gtk_main();
	
//	g_print("file %s",argv[1]);
	
	exit(0);				
}
