/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
 
#include "punto.h"
#include "screen.h"
#include "GL/gl.h"
#include <gnome.h> 
#include "comandos.h"
#include "matriz_op.h"

/**********************************************
* Nombre: jccCalculaDeterminanted4                     
*															  
* Comentarios: calcula el determinante de una matriz 
* 
* Salida:determinante 
**********************************************/

double jccCalculaDeterminanted4(double *matrix)
{
	double determinante=0;
	int i;

	for(i=0;i<4;i+=2)
	{
		determinante+=(jccCalculaMenor(i,matrix)*matrix[i]);
	}
	
	for(i=1;i<4;i+=2)
	{
		determinante-=(jccCalculaMenor(i,matrix)*matrix[i]);
	}
	
return determinante;
}


/**********************************************
* Nombre: jccCalculaDeterminanted3                     
*															  
* Comentarios: calcula el determinante de una matriz 
* 
* Salida:determinante 
**********************************************/

double jccCalculaDeterminanted3(double *matrix)
{
	double determinante;

	determinante=(matrix[0]*matrix[4]*matrix[8])+
						(matrix[1]*matrix[5]*matrix[6])+
						(matrix[2]*matrix[3]*matrix[7])-
						(matrix[2]*matrix[4]*matrix[6])-
						(matrix[0]*matrix[5]*matrix[7])-
						(matrix[1]*matrix[3]*matrix[8]);

return determinante;
}

/**********************************************
* Nombre: calcula_menor                     
*															  
* Comentarios: calcula el menor de un elemento de una matriz 
* 
* Salida: menor de ese elemento 
**********************************************/

double jccCalculaMenor(int elemento,double *matrix)
{
	double adj[10];
	double menor,div,div2;
	int i,j=0;
	
	for(i=0;i<16;i++)
	{
		div=(double)i/4;
		div2=(double)elemento/4;
		
		if((int)div!=(int)(div2)&&((div)-(int)(div)!=div2-(int)div2))
			{
			adj[j]=matrix[i];
			j++;
			}
	}
	
	menor=jccCalculaDeterminanted3(adj);

	
	return menor;
}



/**********************************************
* Nombre: calculaInversa                     
*															  
* Comentarios: calcula el menor de un elemento de una matriz 
* 
* Salida: menor de ese elemento 
**********************************************/

double *jccCalculaInversa(double *matrix)
{
	double *matrix_i,determinante;
	int i=0,j=0,signo=1;
		
	matrix_i=malloc(sizeof(double)*16);

	determinante=jccCalculaDeterminanted4(matrix);

	
	for(i=0;i<4;i++)
	{
		for(j=0;j<4;j++)
		{
			matrix_i[4*j+i]=jccCalculaMenor(4*i+j,matrix)/determinante*signo;
			signo*=-1;
		}
	signo*=-1;
	}
	
	return matrix_i;
}


/***********************************************/
// Nombre: mul_matrix                     
//															  
// Comentarios: multiplica dos matrices 
// 
// Salida: matriz producto 
/**********************************************/

double *mul_matrix(double *matr1,double *matr2)
{
	double *matr_s;
	int i;

	matr_s=malloc(sizeof(double)*16);
	
	for(i=0;i<=12;i+=4)
	{
	matr_s[0+i]=(matr1[0+i]*matr2[0])+(matr1[1+i]*matr2[4])+
				 (matr1[2+i]*matr2[8])+(matr1[3+i]*matr2[12]);

	matr_s[1+i]=(matr1[0+i]*matr2[1])+(matr1[1+i]*matr2[5])+
				 (matr1[2+i]*matr2[9])+(matr1[3+i]*matr2[13]);

	matr_s[2+i]=(matr1[0+i]*matr2[2])+(matr1[1+i]*matr2[6])+
				 (matr1[2+i]*matr2[10])+(matr1[3+i]*matr2[14]);

	matr_s[3+i]=(matr1[0+i]*matr2[3])+(matr1[1+i]*matr2[7])+
				 (matr1[2+i]*matr2[11])+(matr1[3+i]*matr2[15]);
	}
	
	return matr_s;
}


/***********************************************/
// Nombre: transforma_punto                     
//															  
// Comentarios: transforma un punto segun una
//		matriz de transformacion
// 
// Salida: el punto transformado 
/**********************************************/

punto transforma_punto(double *matrix_t,punto pto)
{
	punto ptot;

	ptot.x=(matrix_t[0]*pto.x)+(matrix_t[1]*pto.y)+(matrix_t[2]*pto.z)+matrix_t[3];
	ptot.y=(matrix_t[4]*pto.x)+(matrix_t[5]*pto.y)+(matrix_t[6]*pto.z)+matrix_t[7];
	ptot.z=(matrix_t[8]*pto.x)+(matrix_t[9]*pto.y)+(matrix_t[10]*pto.z)+matrix_t[11];
	
	return ptot;
}

/***********************************************/
// Nombre: transforma_punto                     
//															  
// Comentarios: transforma un punto segun una
//		matriz de transformacion
// 
// Salida: el punto transformado 
/**********************************************/

void asocia_matriz_identidad(double *matrix)
{
	matrix[0]=1;
  	matrix[1]=0;
  	matrix[2]=0.0f;
 	matrix[3]=0.0f;
  	matrix[4]=0;
  	matrix[5]=1;
  	matrix[6]=0.0f;
  	matrix[7]=0.0f;
  	matrix[8]=0.0f;
  	matrix[9]=0.0f;
  	matrix[10]=1;
  	matrix[11]=0.0f;
  	matrix[12]=0;
  	matrix[13]=0;
	matrix[14]=0.0f;
  	matrix[15]=1;
}


/***********************************************/
// Nombre: suma_vectorial                     
//															  
// Comentarios: suma dos vectores
// 
// Salida: el punto con la suma 
/**********************************************/

punto suma_vectorial(punto v1,punto v2)
{
	punto suma;

	suma.x=v2.x+v1.x;
	suma.y=v2.y+v1.y;
	suma.z=v2.z+v1.z;

	return suma;
}

/***********************************************/
// Nombre: resta_vectorial                     
//															  
// Comentarios: resta dos vectores
// 
// Salida:  punto con la resta 
/**********************************************/

punto resta_vectorial(punto v1,punto v2)
{
	punto resta;

	resta.x=v1.x-v2.x;
	resta.y=v1.y-v2.y;
	resta.z=v1.z-v2.z;

	return resta;
}

/***********************************************
* Nombre:invierte_matrix                     
*															  
* Comentarios: multiplica dos matrices 
* 
* Salida: matriz producto 
**********************************************/


double *invierte_matrix(double *matr)
{

//	return matr_s;
}
