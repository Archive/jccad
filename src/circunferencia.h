/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
	
#ifndef _CIRCUNFERENCIA_H
#define _CIRCUNFERENCIA_H

#include <glib.h>
#include "metodos.h"
#include "math.h"
#include <dlfcn.h>

//circunferencia que pasa por 3 puntos
figura circunferencia_3puntos(figura fig,punto *ptos,int orden)
{
	
	double eje_x,eje_y,radio;
	punto m1,m2,centro,a,b;

	eje_x=ptos[1].x-ptos[0].x;
	eje_y=ptos[1].y-ptos[0].y;

	if(!eje_x){eje_x=0;}
	if(!eje_y){eje_y=0;}

	//calcular el centro de la circunferencia
	m1.x=(ptos[0].x+ptos[1].x)/2;
	m1.y=(ptos[0].y+ptos[1].y)/2;

	m2.x=(ptos[0].x+ptos[2].x)/2;
	m2.y=(ptos[0].y+ptos[2].y)/2;

	a.x=ptos[0].x-m1.x;
	a.y=ptos[0].y-m1.y;
	b.x=ptos[0].x-m2.x;
	b.y=ptos[0].y-m2.y;

	centro.y=((a.x*((b.x*m2.x)+(b.y*m2.y)))-b.x*(((a.x*m1.x)+(a.y*m1.y))))/((b.y*a.x)-(a.y*b.x));
	centro.x=((a.x*m1.x)+(a.y*m1.y)-(centro.y*a.y))/(a.x);

	centro.z=0;

	fig.centro=centro;

	//y el radio
	radio=sqrt(pow(centro.x-ptos[0].x,2)+pow(centro.y-ptos[0].y,2));

	fig.base_fx.b_monomio[0]=centro.x;
	fig.base_fy.b_monomio[0]=centro.y;
	fig.base_fz.b_monomio[0]=centro.z;

	fig.base_fy.seno[1]=radio;
	fig.base_fx.coseno[1]=radio;

	fig.orden=1;

	return fig;
}


figura circ(figura fig,punto *ptos,int orden)
{
	double eje_x,eje_y,radio;
	
	eje_x=ptos[1].x-ptos[0].x;
	eje_y=ptos[1].y-ptos[0].y;

	if(!eje_x){eje_x=0;}
	if(!eje_y){eje_y=0;}
	
	radio=sqrt(pow(eje_x,2)+pow(eje_y,2));


	fig.orden=1;
	fig.base_fx.b_monomio[0]=ptos[0].x;
	fig.base_fy.b_monomio[0]=ptos[0].y;

	fig.base_fy.seno[1]=radio;
	fig.base_fx.coseno[1]=radio;
	
	fig.centro=ptos[0];
	return fig;
}


void new_circle(void)
{
	GList *lista;
	int com;
	figura *fig;
	
	fig=crea_figura();
	lista_figuras=g_list_append(lista_figuras,fig);
	
	fig->parametros_generadores=malloc(sizeof(punto)*4);

	//dando color
	fig->color[0]=shmptr->color_r;
	fig->color[1]=shmptr->color_g;
	fig->color[2]=shmptr->color_b;

	fig->correc=1;
	fig->n_nodo=8;
	fig->s_nodo=0;
	fig->e_nodo=8;

	//fig->TIPO=circunferencia;


	//fig->n_options=0;
	fig->num_parametros_generadores=0;

	fig->func=circ;
	sprintf(fig->func_name,"circ");	
	
	do{
		com=fig->num_opciones_generadoras+fig->num_parametros_generadores;
		texto_a_GUI("Input center point or type (01:Circunferencia 3 puntos)",NULL);
		get_options_from_input(com,fig);
		get_parameters_from_input(fig->func,com,fig);
		get_parameters_from_mouse(fig->func,fig,com);
		}while(com==0);

	switch(fig->opciones_generadoras[0])
		{
		case 0:
			do
			{
			//comprueba el numero de parametros_generadores recogidos en fig
			com=fig->num_parametros_generadores;
			switch(com)
				{
				case 0:
					texto_a_GUI("Input center point",NULL);
					break;
				case 1:
					texto_a_GUI("Input point number %d",com);
					break;
				}
			//cogemos los parametros_generadores introducidos desde la linea de comandos
			get_parameters_from_input(fig->func,com,fig);
			//y tambien desde el raton
			get_parameters_from_mouse(fig->func,fig,com);
			}while(com<=1);

		break;

	case 1:

		fig->func=circunferencia_3puntos;

		do
		{
			//comprueba el numero de parametros_generadores recogidos en fig
			com=fig->num_parametros_generadores;
			texto_a_GUI("Input point number %d",com);
			//cogemos los parametros_generadores introducidos desde la linea de comandos
			get_parameters_from_input(circunferencia_3puntos,com,fig);
			//y tambien desde el raton
			get_parameters_from_mouse(fig->func,fig,com);
		}while(com<=2);
	break;
	}
	//sprintf(shmptr->ret_msg,"Introduce nuevo comando");

//	fig->num_parametros_generadores--;

	add_snaps(fig);
	add_snap_center_point(fig);		
	proyecta_snap_points();
	return fig;
}
#endif
