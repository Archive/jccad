#ifndef _METODOS_H
#define _METODOS_H

#include "figura.h"
#include "circunferencia.h"
#include "linea.h"
#include "spline.h"
#include "point.h"
#include "polilinea.h"
#include "arco.h"
#include "move.h"
#include "delete.h"
#include "elipse.h"
#include "copy.h"
#include "poligono.h"
#include "rotate.h"

void get_options_from_input(int com,figura *fig);
void get_parameters_from_mouse(figura (*func)(),figura *fig,int com);
void get_parameters_from_input(figura (*func)(),int com,figura *fig);
void texto_a_GUI(char *msg,int param);
#endif
