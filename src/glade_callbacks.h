/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#ifndef _GLADE_CALLBACKS_H
#define _GLADE_CALLBACKS_H
#include <glade/glade.h>
#include "comandos.h"

GtkWidget *colorpicker;
GtkWidget *textview;

//finaliza el programa

void finalizar(void)
{
	gtk_main_quit();
   shmptr->quit=1;
}

void on_about_activate(void)
{
	GladeXML *about;
	about= glade_xml_new (JCC_AUX_DIR"/about.glade",NULL,NULL);
}



//Esta funcion recoge el color seleccionado 
void on_colorpicker1_clicked (GtkButton *button, gpointer user_data) 
{
	gushort r,g,b,a;
	shmptr->comando=0;
	gnome_color_picker_get_i16 (colorpicker,&r,&g,&b,&a);
	shmptr->color_r=(double)r/65535;
	shmptr->color_g=(double)g/65535;
	shmptr->color_b=(double)b/65535;
}

//Las siguientes funciones recoen el tipo de figura que se
//ha seleccionado
void on_arco_clicked (GtkButton *button, gpointer user_data) 
{
	jcc_comandos_exec("arco");
}

void on_linea_clicked (GtkButton *button, gpointer user_data) 
{
	jcc_comandos_exec("line");
}

void on_point_clicked (GtkButton *button, gpointer user_data) 
{
	jcc_comandos_exec("point");
}

void on_circunf_clicked (GtkButton *button, gpointer user_data) 
{
	jcc_comandos_exec("circle");
}

void on_poligon_clicked (GtkButton *button, gpointer user_data) 
{
	jcc_comandos_exec("poligon");
}

void on_elipse_clicked (GtkButton *button, gpointer user_data) 
{
	jcc_comandos_exec("elipse");
}

void on_poliline_clicked (GtkButton *button, gpointer user_data) 
{
	jcc_comandos_exec("pline");
}

void on_spline_clicked (GtkButton *button, gpointer user_data) 
{
	jcc_comandos_exec("spline");
}

void on_move_clicked (GtkButton *button, gpointer user_data) 
{
	jcc_comandos_exec("move");
}

void on_rotation_clicked (GtkButton *button, gpointer user_data) 
{
	jcc_comandos_exec("rotate");
}

//Entrada de comandos
void on_textview1_add(GtkTextView *text, gpointer user_data) 
{
	GtkTextBuffer *buffer;
	GtkTextIter begin, end;
	char *texto;


	buffer=gtk_text_view_get_buffer (text);
	gtk_text_buffer_get_start_iter (buffer, &begin);
	gtk_text_buffer_get_end_iter (buffer, &end);
	texto=gtk_text_buffer_get_text(buffer, &begin, &end, TRUE);
	parse_text(texto);
	sprintf(texto,"");
	gtk_text_buffer_set_text(buffer,texto,0);
}

//snap options callback
void on_intersection_toggled(GtkCheckButton *cbutton, gpointer user_data)
{
	switch(shmptr->snap_intersection)
	{
	case 0:
	shmptr->snap_intersection=1;
	break;
	case 1:
	shmptr->snap_intersection=0;
	break;
	}	
}

void on_endpoints_toggled(GtkCheckButton *cbutton, gpointer user_data)
{
	switch(shmptr->snap_end_point)
	{
	case 0:
	shmptr->snap_end_point=1;
	break;
	case 1:
	shmptr->snap_end_point=0;
	break;
	}	
}


void on_midpoint_toggled(GtkCheckButton *cbutton, gpointer user_data)
{
	switch(shmptr->snap_mid_point)
	{
	case 0:
	shmptr->snap_mid_point=1;
	break;
	case 1:
	shmptr->snap_mid_point=0;
	break;
	}	
}

void on_orto_toggled(GtkCheckButton *cbutton, gpointer user_data)
{
	switch(shmptr->snap_orto)
	{
	case 0:
	shmptr->snap_orto=1;
	break;
	case 1:
	shmptr->snap_orto=0;
	break;
	}	
}

void on_center_toggled(GtkCheckButton *cbutton, gpointer user_data)
{
	switch(shmptr->snap_center_point)
	{
	case 0:
	shmptr->snap_center_point=1;
	break;
	case 1:
	shmptr->snap_center_point=0;
	break;
	}	
}

void on_grid_toggled(GtkCheckButton *cbutton, gpointer user_data)
{
	switch(shmptr->snap_s_point)
	{
	case 0:
	shmptr->snap_s_point=1;
	//add_snap_grid_point(5);
	break;
	case 1:
	shmptr->snap_s_point=0;
	break;
	}	
}

void on_spinbutton1_changed(GtkSpinButton *sbutton, gpointer user_data)
{
	shmptr->lwidth=(int)gtk_spin_button_get_value(sbutton);	
}

void on_copy1_activate(void)
{
	jcc_comandos_exec("copy");
}

void on_copiar1_activate(void)
{
	jcc_comandos_exec("copy");
}

void on_save_as1_activate(void)
{
}


void on_save1_activate(void)
{
}

void on_preferences1_activate(void)
{
}

void on_properties1_activate(void)
{
}


void on_new1_activate(void)
{
}


void on_estirar1_activate(void)
{
}


void on_girar1_activate(void)
{
	jcc_comandos_exec("rotate");
}


void on_open1_activate(void)
{
}


void on_paste1_activate(void)
{
}

void on_quit1_activate(void)
{
}

void on_cut1_activate(void)
{
}

void on_delete1_activate(void)
{
	jcc_comandos_exec("delete");
}

void on_clear1_activate(void)
{
}
#endif
