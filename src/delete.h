/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
 
#ifndef _DELETE_H
#define _DELETE_H

#include <glib.h>
#include "metodos.h"
#include "math.h"
#include "init.h"
#include "SDL.h"

figura *new_delete(void)
{
	GList *lista;
	figura *fig;
	int com;
	
	lista=g_list_first(lista_seleccion);
	lista=g_list_next(lista);

	desactiva_dibujo();
	SDL_Delay(30);	
	
	while(lista!=NULL)
	{	
		fig=lista->data;
		lista=g_list_remove(lista,fig);
		lista_figuras=g_list_remove(lista_figuras,fig);
		destruye_figura(fig);
	}

	
	activa_dibujo();

	texto_a_GUI("Borrando figuras, espero que sepas los que estas haciendo...");
}
#endif
