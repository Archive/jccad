/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
 
#ifndef _MATRIZ_OP_H
#define _MATRIZ_OP_H

#include "punto.h"

//metodos
double jccCalculaDeterminanted4(double *matrix);
double *jccCalculaInversa(double *matrix);
double jccCalculaMenor(int elemento,double *matrix);
punto transforma_punto(double *matrix_t,punto pto);
double *mul_matrix(double *matr1,double *matr2);
double *invierte_matrix(double *matr);
void asocia_matriz_identidad(double *matrix);
punto suma_vectorial(punto v1,punto v2);
punto resta_vectorial(punto v1,punto v2);
#endif
