/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
 
#include "bloque.h"
#include <gnome.h> 
#include "auxiliares.h"
#include "listas.h"
#include "screen.h"

/**********************************************
 Function: jcc_bloque_new                     
**********************************************/
bloque *jcc_bloque_new(void)
{
	bloque *bloque;
	
	bloque=malloc(sizeof(bloque));
	bloque->lista=g_list_alloc();
	
	return bloque;
}

/**********************************************
 Function: jcc_bloque_addFigure                     
**********************************************/

void jcc_bloque_addFigure(bloque *bloque,figura *fig)
{
	GList *lista_figura,*lista_bloque;

	lista_bloque=bloque->lista;
	lista_figura=fig->bloques;

	lista_bloque=g_list_append(lista_bloque,fig);		
	lista_figura=g_list_append(lista_figura,bloque);

}

/**********************************************
 Function: jcc_bloque_move                    
**********************************************/
void jcc_bloque_move(bloque *bloque,punto pto)
{
	GList *lista;
	figura *fig;
	
	lista=bloque->lista;
	lista=g_list_next(lista);
	while(lista!=NULL)
	{
		fig=lista->data;
		
		fig->matriz_translacion[3]=pto.x;
		fig->matriz_translacion[7]=pto.y;
		fig->matriz_translacion[11]=pto.z;
		
		fig->matriz_escala[0]=screen_info.zoom/200;
		fig->matriz_escala[5]=screen_info.zoom/200;
		fig->matriz_escala[10]=screen_info.zoom/200;

		calcula_puntos_precalculados(fig,0,fig->longitud);
		lista=g_list_next(lista);
	}
}

/**********************************************
 Function: jcc_bloque_delete                    
**********************************************/
void elimina_bloque(bloque *bloque)
{
	GList *lista;
	figura *fig;

	lista=bloque->lista;
	lista=g_list_next(lista);
	
	while(lista!=NULL)
	{
		fig=lista->data;
		
			g_list_remove(lista_auxiliar,fig);
			g_list_remove(lista_figuras,fig);
			destruye_figura(fig);
		
		lista=g_list_next(lista);
		}
	

	g_list_free(bloque->lista);
	free (bloque);	

}

/**********************************************
 Function: jcc_bloque_copy                    
**********************************************/
bloque *jcc_bloque_copy(bloque *b1,GList *lista)
{
	bloque *b2;
	GList *lista1;
	figura *fig1,*fig2;
	
	b2=jcc_bloque_new();
	
	lista1=b1->lista;
	lista1=g_list_next(lista1);
	
	while(lista1!=NULL)
	{
		fig1=lista1->data;
		fig2=copia_figura(fig1,lista);
		jcc_bloque_addFigure(b2,fig2);
		lista1=g_list_next(lista1);
	}
	
	return b2;
}
