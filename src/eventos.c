/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#include "init.h"
#include "comandos.h"
#include <gnome.h> 
#include <SDL.h> 
#include "figura.h"
#include "punto.h"
#include "screen.h"
#include "snap.h"
#include "select.h"
#include "auxiliares.h"


//dec privadas

void comprobar_raton(SDL_Event event,Uint8 *keys);
void comprobar_teclado(Uint8 *keys);


void mouse_b_up_case(SDL_Event event,Uint8 *keys)
{
//seleccionamos la figura
			
			if(screen_info.select_state==1)
			{
				screen_info.select_point_end=request_screen_point(event);
				screen_info.select_state=2;
				
				if (keys[SDLK_LSHIFT])
					screen_info.select_state=3;
			}
}

/***********************************************/
// Nombre: comprobar_eventos                     
//															  
// Comentarios: Control de eventos
// 
// Salida: ninguna 
/**********************************************/

void comprobar_eventos(void)
{
	SDL_Event event;
	Uint8 *keys;

	SDL_PollEvent(&event);
	keys = SDL_GetKeyState(NULL);

	
	if(event.type==SDL_KEYUP)
			proyecta_snap_points();
	
	//comprobar teclado
	comprobar_teclado(keys);
	
	//comprobar raton 
	if(shmptr->comando==0)
		comprobar_raton(event,keys);
	
	SDL_Delay(1);
	
}

void comprobar_raton(SDL_Event event,Uint8 *keys)
{
	static snap seleccionado;
	static punto posic;
	int selected_fig,selected_node;
	static int leave;

	switch (event.type)
	{
		case SDL_MOUSEBUTTONDOWN:

			if(leave==0)
			{
			leave=1;
			screen_info.drawing=1;
			posic=request_point(event);	
			seleccionado=snap_control_point(&posic);
			screen_info.drawing=0;
			selected_fig=seleccionado.ID_FIGURA[0];
			selected_node=seleccionado.ID_FIGURA[1];

			
			if(comprueba_figura(selected_fig,lista_figuras)==1)
			 	{
					set_parameter(selected_fig,selected_node);
					screen_info.select_state=0;
				}else if(screen_info.select_state==0)
					{
					screen_info.select_state=1;
					//y recogemos el primer punto de seleccion
					screen_info.select_point_begin=request_screen_point(event);
					}
			}
			
		break;

		case SDL_MOUSEMOTION:
			//actualizamos guias
				posic=request_point(event);
				snap_control_point(&posic);
				screen_info.select_point_end=request_screen_point(event);
				jcc_aux_move(cursor,posic);
				leave=0;
		break;

		case SDL_MOUSEBUTTONUP:
				mouse_b_up_case(event,keys);
				leave=0;
			break;
	}

}

void comprobar_teclado(Uint8 *keys)
{

	if (keys[SDLK_ESCAPE])
	{
		//	done = 1;
		//	free(back);
	}
	if (keys[SDLK_z])
	{
		screen_info.zoom*=1.01;// +=screen_info.zoom/(100); 
			//el denominador aqui representa la velocidad de zoom
		}
	if (keys[SDLK_a])
		{
			screen_info.zoom/=1.01;// -=screen_info.zoom/(100); 
		}
	//GIRAR CAMARA
	
	if (keys[SDLK_1])
		{
			screen_info.angle[0]=90;
			screen_info.angle[1]=0;
			screen_info.angle[2]=0;
		}
	if (keys[SDLK_2])
		{
			screen_info.angle[0]=0;
			screen_info.angle[1]=90;
			screen_info.angle[2]=0;
		}
	if (keys[SDLK_3])
		{
			screen_info.angle[0]=0;
			screen_info.angle[1]=0;
			screen_info.angle[2]=0;
		}

	if (keys[SDLK_4])
		{
			screen_info.angle[0]=30;
			screen_info.angle[1]=0;
			screen_info.angle[2]=45;
		}
	
	if (keys[SDLK_q])
		{
			screen_info.angle[0] +=0.2;
		
		}
	if (keys[SDLK_w])
		{
			screen_info.angle[0] -=0.2;
		}
	if (keys[SDLK_e])
		{
			screen_info.angle[1] +=0.2;
		}
	if (keys[SDLK_r])
		{
			screen_info.angle[1] -=0.2;
		}
	if (keys[SDLK_t])
		{
			screen_info.angle[2] +=0.2;
		}
	if (keys[SDLK_y])
		{
			screen_info.angle[2] -=0.2;
		}
	
	if (keys[SDLK_UP])
		{
			screen_info.scroll_y +=0.01*screen_info.zoom;
		}
	if (keys[SDLK_RIGHT])
		{
			screen_info.scroll_x +=0.01*screen_info.zoom;
		}
	if (keys[SDLK_LEFT])
		{
			screen_info.scroll_x -=0.01*screen_info.zoom;
		}
	if (keys[SDLK_DOWN])
		{
			screen_info.scroll_y -=0.01*screen_info.zoom;
		}
	
	if (keys[SDLK_p])
		{
			proyecta_snap_points();
		}

}
