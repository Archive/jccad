/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#ifndef _SELECT_H
#define _SELECT_H

#include <gnome.h>
#include "snap.h"
#include "figura.h"

GList *control_s;

void das(int num);
void dibuja_seleccion(void);
void select_fig(GList *lista);
void deselect_fig(void);
void set_parameter(figura *fig,int nodo);
int get_node_from_mouse(figura (*func)(),figura *fig,int com);
#endif
