/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#ifndef _SPLINE_H
#define _SPLINE_H

#include <glib.h>
#include "metodos.h"
#include "math.h"
#include "SDL.h"

figura cspline(figura fig,punto *ptos,int orden)
{
int i,j,k,nodo_inicio;
double a,b,c,d,l,m,t1,t2;

orden=fig.e_nodo;

fig.orden=3;
fig.longitud=fig.e_nodo;
fig.n_nodo=fig.e_nodo;
fig.s_nodo=0;
fig.is_spline=1;

//calculamos cada tramo

//primero para x(t) y luego para y(t)

if(orden>=2&&shmptr->comando==7) nodo_inicio=orden-2; 
	else nodo_inicio=0; 

for(i=nodo_inicio;i<=fig.e_nodo;i++)
{
//cogiendo 3 puntos
//calculamos las tangentes en los puntos interiores
if(i>0) t1=fig.base_fx.cubic[i-1].T2;
	else t1=0;
if(i<orden-1) t2=(ptos[i+2].x-ptos[i].x)/2;
	else t2=(ptos[i+1].x-ptos[i].x)/2;

l=ptos[i].x;
m=ptos[i+1].x;

//calculamos los coeficientes
d=t1+t2+2*(l-m);
c=-(t1-m+l)-(((3*i)+1)*d);
b=(m-l)-(((2*i)+1)*c)-(((3*i*i)+(3*i)+1)*d);
a=l-(i*i*i*d)-(i*i*c)-(b*i);

fig.base_fx.cubic[i].T2=t2;
fig.base_fx.cubic[i].b_monomios[0]=a;
fig.base_fx.cubic[i].b_monomios[1]=b;
fig.base_fx.cubic[i].b_monomios[2]=c;
fig.base_fx.cubic[i].b_monomios[3]=d;
}

for(i=nodo_inicio;i<=fig.e_nodo;i++)
{
//cogiendo 3 puntos
//calculamos las tangentes en los puntos interiores
if(i>0) t1=fig.base_fy.cubic[i-1].T2;
	else t1=0;
if(i<orden-1) t2=(ptos[i+2].y-ptos[i].y)/2;
	else t2=(ptos[i+1].y-ptos[i].y)/2;

l=ptos[i].y;
m=ptos[i+1].y;

//calculamos los coeficientes
d=t1+t2+2*(l-m);
c=-(t1-m+l)-(((3*i)+1)*d);
b=(m-l)-(((2*i)+1)*c)-(((3*i*i)+(3*i)+1)*d);
a=l-(i*i*i*d)-(i*i*c)-(b*i);

fig.base_fy.cubic[i].T2=t2;
fig.base_fy.cubic[i].b_monomios[0]=a;
fig.base_fy.cubic[i].b_monomios[1]=b;
fig.base_fy.cubic[i].b_monomios[2]=c;
fig.base_fy.cubic[i].b_monomios[3]=d;
}

for(i=nodo_inicio;i<=fig.e_nodo;i++)
{
//cogiendo 3 puntos
//calculamos las tangentes en los puntos interiores
if(i>0) t1=fig.base_fz.cubic[i-1].T2;
	else t1=0;
if(i<orden-1) t2=(ptos[i+2].z-ptos[i].z)/2;
	else t2=(ptos[i+1].z-ptos[i].z)/2;

l=ptos[i].z;
m=ptos[i+1].z;

//calculamos los coeficientes
d=t1+t2+2*(l-m);
c=-(t1-m+l)-(((3*i)+1)*d);
b=(m-l)-(((2*i)+1)*c)-(((3*i*i)+(3*i)+1)*d);
a=l-(i*i*i*d)-(i*i*c)-(b*i);

fig.base_fz.cubic[i].T2=t2;
fig.base_fz.cubic[i].b_monomios[0]=a;
fig.base_fz.cubic[i].b_monomios[1]=b;
fig.base_fz.cubic[i].b_monomios[2]=c;
fig.base_fz.cubic[i].b_monomios[3]=d;
}

return fig;
}




void new_spline(void)
{
	int com;
	Uint8 *keys;
	figura *fig;
	
	fig=crea_figura();
	lista_figuras=g_list_append(lista_figuras,fig);
	

	fig->parametros_generadores=malloc(sizeof(punto));
	fig->base_fx.cubic=malloc(sizeof(cubic_spl));
	fig->base_fy.cubic=malloc(sizeof(cubic_spl));
	fig->base_fz.cubic=malloc(sizeof(cubic_spl));

	fig->color[0]=shmptr->color_r;
	fig->color[1]=shmptr->color_g;
	fig->color[2]=shmptr->color_b;

//	fig->TIPO=spline;
	fig->func=cspline;
	sprintf(fig->func_name,"cspline");	
	
	do{
		keys = SDL_GetKeyState(NULL);
		//comprueba el numero de parametros recogidos en fig
		com=fig->num_parametros_generadores;
		texto_a_GUI("Input point %d",com+1);

		//reservamos memoria suficiente para guardar los parametros
		fig->base_fx.cubic=realloc(fig->base_fx.cubic,(com+2)*sizeof(cubic_spl));
		fig->base_fy.cubic=realloc(fig->base_fy.cubic,(com+2)*sizeof(cubic_spl));
		fig->base_fz.cubic=realloc(fig->base_fz.cubic,(com+2)*sizeof(cubic_spl));
	
		fig->e_nodo=com;
		//y tambien desde el raton
		get_parameters_from_mouse(fig->func,fig,com);
		get_parameters_from_input(fig->func,com,fig);

		}while(!keys[SDLK_ESCAPE]);

		*fig=fig->func(*fig,fig->parametros_generadores,com-1);
		
		fig->e_nodo--;
		calcula_puntos_precalculados(fig,0,fig->longitud);

		add_snaps(fig);
		return fig;
}

#endif
