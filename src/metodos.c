/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */

#include "comandos.h"
#include "punto.h"
#include "SDL.h"
#include "metodos.h"
#include "snap.h"
#include "auxiliares.h"

void get_options_from_input(int com,figura *fig)
{
	//comprobamos si existen nuevos puntos a�adidos desde 
	//la linea de comandos 
		if(shmptr->n_opts_to_read>0)
			{
	//en ese caso lo a�adimos como parametro para crear la figura		
			fig->opciones_generadoras[com]=shmptr->options[shmptr->n_opts_to_read-1];
			fig->num_opciones_generadoras++;
			shmptr->n_opts_to_read--;
			}
	SDL_Delay(0.1);
}


/***********************************************/
// Nombre:get_parameters_from_mouse                      
//															  
// Comentarios: recoge puntos de la pantalla
// y los pasa como parametros_generadores a la figura 
// 
// Salida: ninguna 
/**********************************************/

void get_parameters_from_mouse(figura (*func)(),figura *fig,int com)
{
	
	
	static int button=0;
	SDL_Event event;

	if(shmptr->mouse_enabled==1) return;
	
	SDL_PollEvent(&event);
	//comprueba la entrada
	switch (event.type)
			{
		case SDL_MOUSEBUTTONDOWN:
			//si se ha pulsado el boton del raton
			if(button==0)
				{
				shmptr->ultimo_parametro=fig->parametros_generadores[com];
				//mete el punto como parametro de la figura				
				fig->num_parametros_generadores++;
				//obliga a soltar el boton del raton antes de meter otro parametro
				button=1;
				}
			break;

		//en caso de movimiento
		case SDL_MOUSEMOTION:
			button=0;
			//mete el punto como parametro de la figura				
			fig->parametros_generadores=realloc(fig->parametros_generadores,(com+1)*sizeof(punto));
			fig->parametros_generadores[com]=request_point(event);
			//comprueba los snap points
			get_snap_points(&fig->parametros_generadores[com]);
			jcc_aux_move(cursor,fig->parametros_generadores[com]);
			//para trazar perpendiculares
			snap_orto(fig->parametros_generadores[com-1],&fig->parametros_generadores[com]);
			//refresca la salida		
			*fig=func(*fig,fig->parametros_generadores,com);
			calcula_puntos_precalculados(fig,0,fig->longitud);
			break;
		}	

			SDL_Delay(1);
}


/***********************************************/
// Nombre:texto a GUI                     
//															  
// Comentarios: saca informacion al GUI
// 
// Salida: ninguna 
/**********************************************/

void texto_a_GUI(char *msg,int param)
{
	if(shmptr->mouse_enabled==1) return;

	sprintf(shmptr->ret_msg,msg,param);
	shmptr->is_msg=1;
}


/***********************************************/
// Nombre:get_parameters_from_input                      
//															  
// Comentarios: recoge puntos desde la linea de
//		comandos
// 
// Salida: ninguna 
/**********************************************/

void get_parameters_from_input(figura (*func)(),int com,figura *fig)
	{
	int i=0;
	//comprobamos si existen nuevos puntos a�adidos desde 
	//la linea de comandos 
		if(shmptr->n_param_to_read>0)
			{
			
			
			fig->parametros_generadores=realloc(fig->parametros_generadores,(shmptr->n_param_to_read+com+1)*sizeof(punto));
			do
			{	
				//en ese caso lo a�adimos como parametro para crear la figura		
				fig->parametros_generadores[com+i]=shmptr->parametros[i];

				shmptr->n_param_to_read--;
				//fig->e_nodo=fig->num_parametros_generadores;
				fig->num_parametros_generadores++;
				*fig=func(*fig,fig->parametros_generadores,com);
				i++;
			}while(shmptr->n_param_to_read>0);
			
			calcula_puntos_precalculados(fig,0,fig->longitud);
			}
	}


