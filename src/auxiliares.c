/* Copyright (C) 2003-2004 Jos� Carlos Marcos del Blanco.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Jos� Carlos Marcos del Blanco <jcmarcos@copenhague.info>
 */
 
#include "auxiliares.h"
#include "listas.h"
#include <GL/gl.h>
#include <gnome.h>
#include "bloque.h"
#include "snap.h"
#include "screen.h"

/***********************************************
 Function: jcc_aux_load                     
**********************************************/
bloque *jcc_aux_load(char *file)
{
	bloque *auxiliar;
	auxiliar=lcad_load_file(file,lista_auxiliar);

	return auxiliar;
}


/***********************************************
 Funtion: jcc_aux_draw                     
**********************************************/
void jcc_aux_draw(void)
{
	GList *lista_act;
	figura *fig;
	int i;
	
	lista_act=g_list_next(lista_auxiliar);

	while (lista_act !=NULL)
		{
		fig=lista_act->data;
		glColor4f(fig->color[0], fig->color[1],fig->color[2],fig->color[3]);

		glLineWidth(fig->grosor);
		
		if(fig->patron!=NULL)
			{
			glLineStipple (1,fig->patron);
			}
		else glLineStipple (1,0xFFFF);

		
		glBegin(GL_LINE_STRIP);
			
			for (i = 0; i < fig->num_puntos_precalculados; i++)
			{

				glVertex3f(fig->puntos_precalculados[i].x,
					fig->puntos_precalculados[i].y,
					fig->puntos_precalculados[i].z);
			}
	
		glEnd();
		
		lista_act=g_slist_next(lista_act);
		
		}

}

/***********************************************
 Function: jcc_aux_move                     
**********************************************/
void jcc_aux_move(bloque *bloque,punto pto)
{
	
//if(bloque!=cursor)	
	//g_print("situado:%d",pto);
	
//	bloque->centro=pto;	
	jcc_bloque_move(bloque,pto);	
}

/***********************************************
 Function: jcc_aux_delete                     
**********************************************/
void elimina_auxiliar(bloque *auxiliar)
{
	GList *lista;
	figura *fig;

	lista=auxiliar->lista;
	lista=g_list_next(lista);
	
	while(lista!=NULL)
	{
		fig=lista->data;
		g_list_remove(lista_auxiliar,fig);
		g_list_remove(lista_figuras,fig);
		free(fig->parametros_generadores);
		free(fig->puntos_precalculados);
		free(fig);
		lista=g_list_next(lista);
		}
	
	g_list_free(auxiliar->lista);
	free (auxiliar);	
}
